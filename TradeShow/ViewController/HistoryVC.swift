//
//  HistoryVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/10/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class HistoryVC: UIViewController {

    
   
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var scroll: UIScrollView!
    var tag = Int()

    var dictUserData = NSDictionary()
    var aryForTv = NSMutableArray()
    var aryForScroll = NSMutableArray()
    var lastLocation:CGPoint = CGPoint.zero

    override func viewDidLoad() {
        super.viewDidLoad()
        tv.tableFooterView = UIView()
        tv.estimatedRowHeight = 120.0
        tag = 0
        //--- Get UserData
        let aryTemp = getDataFromLocal(strEntity: "LogInData", strkey: "logindata")
        
        let aryTempdata = NSMutableArray()
        if aryTemp.count > 0 {
            
            
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryTempdata.add(obj.value(forKey: "logindata"))
            }
           // print(aryTempdata)
            dictUserData = NSDictionary()
            dictUserData = aryTempdata.object(at: 0)as! NSDictionary
        }
        
        
        
        //----------- Call EventAPI------------ //Online
        if checkInternetConnection() == true {
            CallEvent_API()
        }
        //------------- Offline--------------
        else{
          
            let aryTemp = getDataFromLocal(strEntity: "EventNameHistory", strkey: "eventnamehistory")
            
            let aryTempOutboxdata = NSMutableArray()
            if aryTemp.count > 0 {
                for j in 0 ..< aryTemp.count {
                    var obj = NSManagedObject()
                    obj = aryTemp[j] as! NSManagedObject
                    aryTempOutboxdata.add(obj.value(forKey: "eventnamehistory"))
                }
               // print(aryTempOutboxdata)
                aryForScroll = NSMutableArray()
                aryForScroll = (aryTempOutboxdata.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
                setUPMenuTap()
                let strID = "\(String(describing: (self.aryForScroll[0]as! NSDictionary).value(forKey: "EventId")!))"

                getEventHistopryFromlocal(strEventID: strID)
            }
            
        }

        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:
    //MARK: -----------ALL IBACTION---------
 
    @IBAction func actionOnBack(_ sender: Any) {
        let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
        appDelegate.window?.rootViewController = mainVcIntial
    }
    //MARK:
    // MARK:-- scrollView Delegate ()--
    
    var _lastContentOffset = CGPoint.zero
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        _lastContentOffset = scrollView.contentOffset
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isEqual(scroll) {
            if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
                self.scroll.contentOffset.y = CGFloat(0)
            }
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if _lastContentOffset.x < CGFloat(scrollView.contentOffset.x) {
            print("Scrolled Right")
        }
        else if _lastContentOffset.x > CGFloat(scrollView.contentOffset.x) {
            print("Scrolled Left")
        }
        else if _lastContentOffset.y < scrollView.contentOffset.y {
            print("Scrolled Down")
        }
        else if _lastContentOffset.y > scrollView.contentOffset.y {
            print("Scrolled Up")
        }

        
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let currentLocation = scrollView.contentOffset
        
        // Add each direction string
        var directionList:[String] = []
        
        if lastLocation.x < currentLocation.x {
            //print("right")
            directionList.append("Right")
        } else if lastLocation.x > currentLocation.x {
            //print("left")
            directionList.append("Left")
        }
        
        // there is no "else if" to track both vertical
        // and horizontal direction
        if lastLocation.y < currentLocation.y {
            //print("up")
            directionList.append("Up")
        } else if lastLocation.y > currentLocation.y {
            //print("down")
            directionList.append("Down")
        }
        
        // scrolled to single direction
        if directionList.count == 1 {
            print("scrolled to \(directionList[0]) direction.")
        } else if directionList.count > 0  { // scrolled to multiple direction
            print("scrolled to \(directionList[0])-\(directionList[1]) direction.")
        }
        
        // Update last location after check current otherwise,
        // values will be same
        lastLocation = scrollView.contentOffset

    }
  
    //MARK:
    //MARK: -----------ALL API CALLING---------
    func CallEventVisitor_API(strEventID : String) {
        
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictUserData.value(forKey: "CompanyId")!))"
            dict.setValue(strEventID, forKey: "EventId")
            dict.setValue(strCompanyID, forKey: "CompanyId")
            print(dict)
            WebService.callAPIBYGET(parameter: dict, url: API_EVENT_VISITOR, OnResultBlock: {  (result, status) in
                print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "EventCompanyVisitorList")as! NSArray
                        
                        let dict = NSMutableDictionary()
                        dict.setValue(strEventID, forKey: "EventId")
                        dict.setValue(aryTemp, forKey: "VisitorList")
                        
                        
                        var strTag = 0
                        //--------ForcheckEvent are exist or not
                        let aryEventHistory = getDataFromLocal(strEntity: "EventHistory", strkey: "history")
                         deleteAllRecords(strEntity: "EventHistory")
                        if aryEventHistory.count > 0 {
                            for j in 0 ..< aryEventHistory.count {
                                var obj = NSManagedObject()
                                let aryTempEventHistory = NSMutableArray()
                                obj = aryEventHistory[j] as! NSManagedObject
                            aryTempEventHistory.add(obj.value(forKey: "history"))
                                let strid = "\(String(describing: (aryTempEventHistory[0] as! NSDictionary).value(forKey: "EventId")!))"
                                if strid == strEventID  {
                                    saveDataInLocalDict(strEntity: "EventHistory", strKey: "history", data: dict)
                                    strTag = 1
                                }else{
                                    saveDataInLocalDict(strEntity: "EventHistory", strKey: "history", data: obj.value(forKey: "history") as! NSMutableDictionary)
                                }
                            }
                            if strTag == 0{
                                saveDataInLocalDict(strEntity: "EventHistory", strKey: "history", data: dict)

                            }
                        }
                        //---------FirstTime---------
                        
                        else{
                            saveDataInLocalDict(strEntity: "EventHistory", strKey: "history", data: dict)
                        }
                        
                        self.aryForTv = NSMutableArray()
                        self.aryForTv = aryTemp.mutableCopy()as! NSMutableArray
                        self.tv.reloadData()
                        if self.aryForTv.count > 0{
                            self.tv.isHidden = false
                        }else{
                          self.tv.isHidden = true
                        }
                        
                        
                        
                        
                       }else{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                        if self.aryForTv.count > 0{
                            self.tv.isHidden = false
                        }else{
                            self.tv.isHidden = true
                        }

                    }
                }
                else{
                    KRProgressHUD.dismiss()
                    iToast.makeText("The request timed out.").show()

                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    func CallEvent_API() {
        
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictUserData.value(forKey: "CompanyId")!))"
            dict.setValue("", forKey: "Status")
            dict.setValue(strCompanyID, forKey: "CompanyId")
            WebService.callAPIBYGET(parameter: dict, url: API_GETEVENT, OnResultBlock: {  (result, status) in
                print(result);
            //KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "EventList")as! NSArray
                        deleteAllRecords(strEntity:"EventNameHistory")
                        saveDataInLocal(strEntity: "EventNameHistory", strKey: "eventnamehistory", data: aryTemp as NSArray)
                        self.aryForScroll = NSMutableArray()
                        self.aryForScroll = aryTemp.mutableCopy()as! NSMutableArray
                        self.setUPMenuTap()
                        if self.aryForScroll.count > 0{
                            let strID = "\(String(describing: (self.aryForScroll[0]as! NSDictionary).value(forKey: "EventId")!))"
                         self.CallEventVisitor_API(strEventID: strID)
                        }
                        
                        
                        self.tv.reloadData()
                      
                        
                    }else{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                    }
                }
                else{
                    KRProgressHUD.dismiss()
                    iToast.makeText("The request timed out.").show()
 
                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    //MARK:- ----------PRIVATE FUNCTION----------
    //MARK:-
    

    func getEventHistopryFromlocal(strEventID : String)  {
        let aryEventHistory = getDataFromLocal(strEntity: "EventHistory", strkey: "history")
        let aryTempEventHistory = NSMutableArray()
        if aryEventHistory.count > 0 {
            for j in 0 ..< aryEventHistory.count {
                var obj = NSManagedObject()
                obj = aryEventHistory[j] as! NSManagedObject
                aryTempEventHistory.add(obj.value(forKey: "history"))
            }
           // print(aryTempEventHistory)
            
        }
        
        let aryfinal = NSMutableArray()
        
        if aryTempEventHistory.count > 0 {
            for i in 0 ..< aryTempEventHistory.count {
                var obj = NSArray()
                let strid = "\(String(describing: (aryTempEventHistory[i] as! NSDictionary).value(forKey: "EventId")!))"
                if strid == strEventID  {
                    obj = (aryTempEventHistory[i] as! NSDictionary).value(forKey: "VisitorList") as! NSArray
                    aryfinal.add(obj)

                }
                
            }
            if aryfinal.count > 0 {
                self.aryForTv = NSMutableArray()
                self.aryForTv = (aryfinal.object(at: 0) as! NSArray).mutableCopy()as! NSMutableArray
                self.tv.reloadData()
                self.tv.isHidden = false

            }else{
               iToast.makeText("Event not found.").show()
               self.tv.isHidden = true
            }
           
        }
        
    }
    
    
    
    func setUPMenuTap(){

        let subViews = self.scroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        self.view.endEditing(true)
        var iWidthButton: CGFloat = 0.0
        var totalx: CGFloat = 0.0
        for i in 0..<aryForScroll.count {
            let btn_Menu = UIButton()
            let lblIndicate = UILabel()
            let strTitle = (aryForScroll[i]as! NSDictionary).value(forKey: "Event")as! String
            btn_Menu.setTitle(strTitle as String?, for: .normal)
            btn_Menu.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.medium)
            btn_Menu.setTitleColor(hexStringToUIColor(hex: "666666"), for: UIControlState.normal)
            let maxLabelSize: CGSize = CGSize(width: 300,height: CGFloat(9999))
            let contentNSString = strTitle as NSString
            let expectedLabelSize = contentNSString.boundingRect(with: maxLabelSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18.0)], context: nil)
            iWidthButton = expectedLabelSize.size.width + 40
            btn_Menu.frame = CGRect(x: totalx, y: 0, width: iWidthButton - 1, height:scroll.frame.size.height)
            lblIndicate.frame = CGRect(x: totalx, y: scroll.frame.size.height - 2  , width: iWidthButton - 1, height:scroll.frame.size.height)
            totalx = totalx + iWidthButton
            self.scroll.layer.cornerRadius = 0.0
            btn_Menu.tag = i
            lblIndicate.tag = i
            btn_Menu.addTarget(self, action: #selector(self.action_ChangeMenu), for: .touchUpInside)
            scroll.addSubview(btn_Menu)
            scroll.addSubview(lblIndicate)
            if i == 0 {
                btn_Menu.setTitleColor(UIColor.white, for: UIControlState.normal)
                lblIndicate.backgroundColor = UIColor.white
                tag = 0
            }else{
                btn_Menu.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                lblIndicate.backgroundColor = UIColor.clear
            }
            
        }
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.scroll.contentSize  = CGSize(width: totalx, height: self.scroll.frame.size.height)
            
               self.scroll.contentOffset.x = CGFloat(0)
           
        }, completion: nil)
        
        self.view.layoutIfNeeded()
        self.viewWillLayoutSubviews()

    }
    @objc func action_ChangeMenu(sender: AnyObject) {
        let subviews : NSArray = self.scroll.subviews as NSArray
        for btn_Menu in subviews{
            if (btn_Menu is UIButton) {
                if (btn_Menu as! UIButton).tag == sender.tag {
                    (btn_Menu as AnyObject).setTitleColor(UIColor.white, for: UIControlState.normal)
                    let strID = "\(String(describing: (self.aryForScroll[sender.tag]as! NSDictionary).value(forKey: "EventId")!))"
                    tag = sender.tag

                    self.aryForTv = NSMutableArray()
                    self.tv.reloadData()
                    if checkInternetConnection() == true {
                        self.CallEventVisitor_API(strEventID: strID)
                    }else{
                        getEventHistopryFromlocal(strEventID: strID)
                    }
                }
                else {
                    (btn_Menu as AnyObject).setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                }
            }else if(btn_Menu is UILabel){
                if (btn_Menu as! UILabel).tag == sender.tag {
                    (btn_Menu as! UILabel).backgroundColor  = UIColor.white
                }
                else {
                    (btn_Menu as! UILabel).backgroundColor  = UIColor.clear
                }
            }
        }
    }
    
    
    
    func action_ChangeSwap(sender: Int  ) {
        
        if sender > aryForScroll.count {
            
        }else{
        
        }
        
        
        
     }
    
}
//MARK:- ----------UITableView Delegate Methods----------
//MARK:-
extension HistoryVC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryForTv.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailVC
            let dictTemp = aryForTv.object(at: indexPath.row)as? NSDictionary
            vc.dictData = dictTemp?.mutableCopy()as! NSMutableDictionary
            vc.strCome = "history"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tv.dequeueReusableCell(withIdentifier: "EventSearchCell", for: indexPath as IndexPath) as! CommonTBLCell
        let dict = aryForTv.object(at: indexPath.row)as! NSDictionary
        
        
        let strname = dict.value(forKey: "Name")as? String
        let strDesignation = dict.value(forKey: "Designation")as? String
        let strEmailId = dict.value(forKey: "EmailId")as? String
        let strContactNo = dict.value(forKey: "ContactNo")as? String
        let strProfileImage = dict.value(forKey: "ProfileImage")as? String
        
        //--Check null conditions
        
        if strname != nil {
            cell.eventS_lblName.text = strname!
            if strname?.characters.count == 0 {
                cell.eventS_lblName.text = " "
            }
        }else{
            cell.eventS_lblName.text = " "
        }
        if strDesignation != nil {
            cell.eventS_lblRole.text = strDesignation!
            if strDesignation?.characters.count == 0 {
                cell.eventS_lblRole.text = " "
            }
            
        }else{
            cell.eventS_lblRole.text = " "
        }
        if strContactNo != nil {
            cell.eventS_lblContactNumber.text = strContactNo!
            if strContactNo?.characters.count == 0 {
                cell.eventS_lblContactNumber.text = " "
            }
        }else{
            cell.eventS_lblContactNumber.text = " "
            
        }
        if strEmailId != nil {
            cell.eventS_Email.text = strEmailId!
            if strEmailId?.characters.count == 0 {
                cell.eventS_Email.text = " "
            }
        }else{
            cell.eventS_Email.text = " "
        }
        
     
    
            let strUrlImage = "\(BaseURL_Image)\(String(describing: strProfileImage!))"
            cell.eventS_img.setImageWith(URL(string: strUrlImage), placeholderImage: UIImage(named: "defult_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
            
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
