//
//  HomeVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/3/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class HomeVC: UIViewController {
    
    @IBOutlet weak var tv: UITableView!
    var aryForTv = NSMutableArray()
    var dictUserData = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        tv.estimatedRowHeight = 135.0
        tv.tableFooterView = UIView()
        
        //--- Get UserData
        let aryTemp = getDataFromLocal(strEntity: "LogInData", strkey: "logindata")
        
        let aryTempdata = NSMutableArray()
        if aryTemp.count > 0 {
           
            
            for j in 0 ..< aryTemp.count {
             var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryTempdata.add(obj.value(forKey: "logindata"))
            }
            print(aryTempdata)
            dictUserData = NSDictionary()
            dictUserData = aryTempdata.object(at: 0)as! NSDictionary
        }
        
        //-- Call EventAPI
        if checkInternetConnection() == true {
            self.CallEvent_API()
        }else{
        
            
            //--- Get eventlist
            let aryTemp = getDataFromLocal(strEntity: "EventList", strkey: "eventlist")
            
            let aryTempOutboxdata = NSMutableArray()
            if aryTemp.count > 0 {
                for j in 0 ..< aryTemp.count {
                    var obj = NSManagedObject()
                    obj = aryTemp[j] as! NSManagedObject
                    aryTempOutboxdata.add(obj.value(forKey: "eventlist"))
                }
                print(aryTempOutboxdata)
                aryForTv = NSMutableArray()
                aryForTv = (aryTempOutboxdata.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
                tv.reloadData()
            
        }
        
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tv.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:
    //MARK:  -----------IBAction-----------
    @IBAction func btnMenu(_ sender : AnyObject) {
        sideMenuVC.toggleMenu()
    }
    
    
    
    //MARK:
    //MARK: -----------ALL API CALLING---------
    func CallEvent_API() {
        
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictUserData.value(forKey: "CompanyId")!))"
            dict.setValue("current", forKey: "Status")
            dict.setValue(strCompanyID, forKey: "CompanyId")
            WebService.callAPIBYGET(parameter: dict, url: API_GETEVENT, OnResultBlock: {  (result, status) in
                print(result);
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "EventList")as! NSArray
                        self.aryForTv = NSMutableArray()
                        self.aryForTv = aryTemp.mutableCopy()as! NSMutableArray
                        self.tv.reloadData()
                        deleteAllRecords(strEntity:"EventList")
                        saveDataInLocal(strEntity: "EventList", strKey: "eventlist", data: aryTemp as NSArray)
                        KRProgressHUD.dismiss()

                    }else{
                        KRProgressHUD.dismiss()

                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                    }
                }
                else{
                    
                    KRProgressHUD.dismiss()

                }
                
            })
        }
        else{
            KRProgressHUD.dismiss()

            iToast.makeText("Please check internet connection.").show()
        }
    }
     
}
//MARK:- ----------UITableView Delegate Methods----------
//MARK:-
extension HomeVC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryForTv.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventVC") as! EventVC
        let dictTemp = aryForTv.object(at: indexPath.row)as? NSDictionary
        vc.dictData = dictTemp?.mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath as IndexPath) as! CommonTBLCell
        let dict = aryForTv.object(at: indexPath.row)as! NSDictionary
        
        let streventname = dict.value(forKey: "Event")as? String
        let streventCompanyname = dict.value(forKey: "CompanyName")as? String
        let streventlocation = dict.value(forKey: "EventLocation")as? String
        var streventstartDate = dict.value(forKey: "EventStartDate")as? String
        var streventEventEndDate = dict.value(forKey: "EventEndDate")as? String
         let streventEventImage = dict.value(forKey: "EventImage")as? String
        
        //Get StartDate & Time
        
        streventstartDate = dateTimeConvertor(str: streventstartDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
        let fullNameArr = streventstartDate?.components(separatedBy: "|")
        let date    = fullNameArr?[0]
        let time = fullNameArr?[1]
        streventEventEndDate = dateTimeConvertor(str: streventEventEndDate!, formet: "yyyy-MM-dd'T'HH:mm:ss.SSS")
        
        //Get EndDate & Time
        
        let fullNameArr1 = streventEventEndDate?.components(separatedBy: "|")
        let date1    = fullNameArr1?[0]
        let time1 = fullNameArr1?[1]
        
        
        if streventname != nil {
            cell.home_lblTitle.text = streventname!
        }
        if streventCompanyname != nil {
            cell.home_lblSubTitle.text = streventCompanyname!
        }
        if streventlocation != nil {
            cell.home_lblLocation.text = "Location - \(String(describing: streventlocation!))"
        }
        cell.home_lblDate.text = "Date: \(String(describing: date!))To \(String(describing: date1!))"
        cell.home_lblTime.text = "Time:\(String(describing: time!)) To\(String(describing: time1!))"
        let strUrlImage = "\(BaseURL_Image)\(String(describing: streventEventImage!))"
        cell.home_imgEvent.setImageWith(URL(string: strUrlImage), placeholderImage: UIImage(named: "defult_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
