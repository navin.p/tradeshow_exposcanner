//
//  RecordAudioVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/5/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import AVFoundation

class RecordAudioVC: UIViewController , AVAudioRecorderDelegate{

    @IBOutlet weak var lblCount: UILabel!
    
    @IBOutlet weak var btnFormaet: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var btnrecord: UIButton!
    weak var handleData: refreshLeaddata?

    
    var audioRecorder: AVAudioRecorder!
    var meterTimer:Timer!
    var isAudioRecordingGranted: Bool!
    //MARK:- ---------Life Cycle---------
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnStop.layer.borderColor = UIColor.black.cgColor
        btnStop.clipsToBounds = true
        btnStop.layer.borderWidth =  1.0
        btnStop.tag = 2
        btnFormaet.layer.borderColor = UIColor.black.cgColor
        btnFormaet.clipsToBounds = true
        btnFormaet.layer.borderWidth =  1.0
        btnFormaet.tag = 3
        btnrecord.layer.borderColor = UIColor.black.cgColor
        btnrecord.clipsToBounds = true
        btnrecord.layer.borderWidth =  1.0
        btnrecord.tag = 1
        btnStop.isEnabled = false
      
        btnStop.setTitleColor(UIColor.lightGray, for: .normal)
        btnrecord.setTitleColor(UIColor.black, for: .normal)
        
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSessionRecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                    }
                }
            }
            break
        default:
            break
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        audioRecorder = nil
    }
    
    //MARK:- ----------ALL IBACTION----------
    //MARK:-
    
    @IBAction func actionOnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnButtomButton(_ sender: UIButton) {
        btnrecord.isEnabled = false
        if sender.tag == 1 {
            startRecord()
            
        }else if(sender.tag == 2){
            finishAudioRecording(success: true)
            
        }else if(sender.tag == 3){
            
        }
    }
    
    func finishAudioRecording(success: Bool) {
        
        audioRecorder.stop()
        audioRecorder = nil
        meterTimer.invalidate()
        
        if success {
            let audioFilename = getDocumentsDirectory().appendingPathComponent("audioRecording.m4a")
            print(audioFilename)
            let audiodata = try? Data(contentsOf: audioFilename)
            DispatchQueue.main.async {
                self.handleData?.refreshData(audioData: audiodata!)
                self.navigationController?.popViewController(animated: true)
                
            }

            print("Recording finished successfully.")
        } else {
            print("Recording failed :(")
        }
    }
    
    @objc func updateAudioMeter(timer: Timer) {
        
        if audioRecorder.isRecording {
            let hr = Int((audioRecorder.currentTime / 60) / 60)
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            let totalTimeString = String(format: "%02d:%02d:%02d", hr, min, sec)
           
            lblCount.text = totalTimeString
            audioRecorder.updateMeters()
            if min > 0 {
                finishAudioRecording(success: true)
            }
        }
    }
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    //MARK:- Audio recoder delegate methods
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
        if !flag {
            finishAudioRecording(success: false)
        }
    }
    func startRecord() {
        btnStop.isEnabled = true
        btnrecord.isEnabled = false
        btnrecord.setTitleColor(UIColor.lightGray, for: .normal)
        btnStop.setTitleColor(UIColor.black, for: .normal)

        if isAudioRecordingGranted == true{
            
            //Create the session.
            let session = AVAudioSession.sharedInstance()
            
            do {
                //Configure the session for recording and playback.
                try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
                try session.setActive(true)
                //Set up a high-quality recording session.
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
                ]
                //Create audio file name URL
                let audioFilename = getDocumentsDirectory().appendingPathComponent("audioRecording.m4a")
                //Create the audio recording, and assign ourselves as the delegate
                audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.record()
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
            }
            catch let error {
                print("Error for start audio recording: \(error.localizedDescription)")
            }
        }
    }
}
