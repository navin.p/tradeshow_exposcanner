//
//  AddNewLeadVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/4/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

import CoreData

protocol refreshLeaddata : class{
    func refreshData(audioData : Data)
    
}
class AddNewLeadVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate ,UIGestureRecognizerDelegate , UITextFieldDelegate {
    
    var dictData = NSMutableDictionary()
    var dictDataVisitorBySearch = NSMutableDictionary()
    
    var imagePicker: UIImagePickerController!
    var strTag = String()
    var strCrossTag = String()
    var scanner = RSScannerViewController()
    
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var img1: UIImageView!
    
    @IBOutlet weak var img2: UIImageView!
    
    @IBOutlet weak var img3: UIImageView!
    
    @IBOutlet weak var img4: UIImageView!
    
    @IBOutlet weak var btnCross1: UIButton!
    
    @IBOutlet weak var btnCross2: UIButton!
    
    @IBOutlet weak var btnCross3: UIButton!
    
    @IBOutlet weak var btnCross4: UIButton!
    
    @IBOutlet weak var txtSearch: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var txtLeadName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtType: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtContactNumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtEmailAddress: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtPosition: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtCompanyName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtCompanySize: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtCountry: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtZipCode: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtComment: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtQrScanner: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var tvSearch: UITableView!

    @IBOutlet weak var btnTransprant: UIButton!
    @IBOutlet weak var viewShowlist: CardView!
    
    @IBOutlet weak var btnTitleOntbl: UIButton!
    
    @IBOutlet weak var btnCloseOntbl: UIButton!
    
    var aryForTv = NSMutableArray()
    var dictUserData = NSDictionary()
    var aryForVisitorData = NSMutableArray()
    var aryForSearchVisitorData = NSMutableArray()

    let aryType = NSMutableArray()
    
    //MARK:- ----------LIFE CYCLE----------
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        aryType.add("Attendee")
        aryType.add("Exhibitors")
      
        txtLeadName.delegate = self
        
        strTag = "1"
        btnCross1.isHidden = true
        btnCross2.isHidden = true
        btnCross3.isHidden = true
        btnCross4.isHidden = true
        
        btnCross1.tag = 1
        btnCross2.tag = 2
        btnCross3.tag = 3
        btnCross4.tag = 4
        
        
        strImageName1 = ""
        strImageName2 = ""
        strImageName3 = ""
        strImageName4 = ""
        strVoiceName = ""
        
        //-------QRCODE Scaner-------
        scanner = RSScannerViewController(cornerView: true, controlView: true, barcodesHandler: { (barcodeObjects) in
            (barcodeObjects as AnyObject).enumerateObjects({ obj, idx, stop in
                DispatchQueue.main.async(execute: {
                    let code = obj as? AVMetadataMachineReadableCodeObject
                 
                    self.refreshQRBarCodeData(strComment: (code?.stringValue)!)

                    DispatchQueue.main.async(execute: {
                        self.scanner.dismiss(animated: true)
                    })
                })
            })
          
        })
        //--- Get UserData
        //--- Get UserData
        let aryTemp = getDataFromLocal(strEntity: "LogInData", strkey: "logindata")
        
        let aryTempdata = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryTempdata.add(obj.value(forKey: "logindata"))
            }
            // print(aryTempdata)
            dictUserData = NSDictionary()
            dictUserData = aryTempdata.object(at: 0)as! NSDictionary
        }
        //print(dictUserData)
        btnTransprant.isHidden = true
        viewShowlist.isHidden = true
        btnTransprant.addTarget(self, action: #selector(self.hidePopUp), for: .touchUpInside)
        
        tv.tableFooterView = UIView()
        self.getAllVisitorImport()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
       // scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1200)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnCross1.layer.cornerRadius = btnCross1.frame.size.width/2
        btnCross1.clipsToBounds = true
        btnCross2.layer.cornerRadius = btnCross2.frame.size.width/2
        btnCross2.clipsToBounds = true
        btnCross3.layer.cornerRadius = btnCross3.frame.size.width/2
        btnCross3.clipsToBounds = true
        btnCross4.layer.cornerRadius = btnCross4.frame.size.width/2
        btnCross4.clipsToBounds = true
        
        btnSearch.layer.cornerRadius = 2
        self.btnNext.layer.borderWidth = 1.0
        self.btnNext.layer.borderColor = UIColor.darkGray.cgColor
        self.btnNext.layer.cornerRadius = 8.0
        self.btnNext.clipsToBounds = true
        
        self.btnCloseOntbl.layer.borderWidth = 1.0
        self.btnCloseOntbl.layer.borderColor = UIColor.darkGray.cgColor
        self.btnCloseOntbl.layer.cornerRadius = 8.0
        self.btnCloseOntbl.clipsToBounds = true
        btnCloseOntbl.addTarget(self, action: #selector(self.hidePopUp), for: .touchUpInside)
        
        
        self.view.endEditing(true)
    }
    
    
    //MARK:- ----------ALL IBACTION----------
    //MARK:-
    
    @IBAction func actionOnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionProfileButton(_ sender: Any) {
        strTag = "1"
        takePhoto()
    }
    
    @IBAction func actiononCardButtom(_ sender: Any) {
        strTag = "2"
        takePhoto()
        
    }
    @IBAction func actionCardFrontbutton(_ sender: Any) {
        strTag = "3"
        takePhoto()
        
    }
    
    @IBAction func actionCardBackButton(_ sender: Any) {
        
        strTag = "4"
        takePhoto()
    }
    
    @IBAction func actionQrcodebutton(_ sender: Any) {
        self.tvSearch.removeFromSuperview()
        scrollView.isScrollEnabled = true
        self.tvSearch.reloadData()
        self.present(scanner, animated: true) {
            
        }
 
        
    }
    
    @IBAction func actionOnMicroButton(_ sender: Any) {
        self.tvSearch.removeFromSuperview()
        scrollView.isScrollEnabled = true
        self.tvSearch.reloadData()
        let recorderView = self.storyboard?.instantiateViewController(withIdentifier: "RecordAudioVC") as! RecordAudioVC
        recorderView.handleData = self
        self.navigationController?.pushViewController(recorderView, animated: true)
    }
    
    @IBAction func actionCrossImage(_ sender: UIButton) {
        self.tvSearch.removeFromSuperview()
        scrollView.isScrollEnabled = true
        self.tvSearch.reloadData()
        if sender.tag == 1 {
            img1.image = UIImage(named: "defult_img")
            btnCross1.isHidden = true
            strImageName1 = ""
        }else if (sender.tag == 2){
            img2.image = UIImage(named: "defult_img")
            btnCross2.isHidden = true
            strImageName2 = ""
        }else if (sender.tag == 3){
            img3.image = UIImage(named: "defult_img")
            btnCross3.isHidden = true
            strImageName3 = ""
            
            
        }else if (sender.tag == 4){
            img4.image = UIImage(named: "defult_img")
            btnCross4.isHidden = true
            strImageName4 = ""
            
        }
    }
    
    @IBAction func actionOnNext(_ sender: Any) {
        self.tvSearch.removeFromSuperview()
        scrollView.isScrollEnabled = true
        self.tvSearch.reloadData()
        if (txtEmailAddress.text?.characters.count)! > 0 {
            if validateEmail(email: txtEmailAddress.text!){
                nextAfterValidation()
            }
            else{
                iToast.makeText("Please enter valid email.").show()
            }
        }else{
            nextAfterValidation()
        }
        
        
    }
    
    @IBAction func actionOnSearch(_ sender: Any) {
        FilterDataAccordingVisitorID(Searching: txtSearch.text! as NSString)
    }
    
    @IBAction func actionOnCompnySize(_ sender: UIButton) {
        self.tvSearch.removeFromSuperview()
        scrollView.isScrollEnabled = true
        self.tvSearch.reloadData()
        if sender.tag == 2{
            tv.tag = 10
            btnTransprant.isHidden = false
            viewShowlist.isHidden = false
            aryForTv = NSMutableArray()
            aryForTv = getCompanySizeList()
            tv.reloadData()
            tv.tableFooterView = UIView()
            btnTitleOntbl.setTitle("SELECT COMPANY SIZE", for: .normal)
        }else if (sender.tag == 1){
            tv.tag = 20
            aryForTv = NSMutableArray()
            aryForTv = aryType.mutableCopy()as! NSMutableArray
            tv.reloadData()
            btnTitleOntbl.setTitle("SELECT TYPE", for: .normal)
            btnTransprant.isHidden = false
            viewShowlist.isHidden = false
        }
    }
    //MARK:- ---------PRIVATE FUNCTION----------
    //MARK:-
    @objc func hidePopUp() {
        btnTransprant.isHidden = true
        viewShowlist.isHidden = true
        
    }
    func nextAfterValidation()  {
        
        DispatchQueue.main.async {
            dictVisitorData = NSMutableDictionary()
            aryImageAll = NSMutableArray()
            let strEventId = "\(String(describing: self.dictData.value(forKey: "EventId")!))"
            image1 = self.img1.image!
            image2 = self.img2.image!
            image3 = self.img3.image!
            image4 = self.img4.image!
            
            dictVisitorData.setValue(strEventId, forKey: "EventId")
            
            dictVisitorData.setValue(strImageName1, forKey: "ProfileImage")
            dictVisitorData.setValue(strImageName2, forKey: "CaptureImage")
            dictVisitorData.setValue(strImageName3, forKey: "CardFrontImage")
            dictVisitorData.setValue(strImageName4, forKey: "CardBackImage")
            dictVisitorData.setValue(strVoiceName, forKey: "VoiceNote")
            
            dictVisitorData.setValue(self.txtSearch.text, forKey: "VisitorIdNo")
            dictVisitorData.setValue(self.txtType.text, forKey: "AttendeeType")
            dictVisitorData.setValue(self.txtLeadName.text, forKey: "Name")
            dictVisitorData.setValue(self.txtContactNumber.text, forKey: "ContactNo")
            dictVisitorData.setValue(self.txtEmailAddress.text, forKey: "EmailId")
            dictVisitorData.setValue(self.txtCompanyName.text, forKey: "VisitorCompany")
            dictVisitorData.setValue("\(self.txtCompanySize.tag)", forKey: "CompanySizeId")
            dictVisitorData.setValue(self.txtCountry.text, forKey: "Country")
            dictVisitorData.setValue(self.txtState.text, forKey: "State")
            dictVisitorData.setValue(self.txtCity.text, forKey: "City")
            dictVisitorData.setValue(self.txtZipCode.text, forKey: "ZipCode")
            dictVisitorData.setValue(self.txtAddress.text, forKey: "Address")
            dictVisitorData.setValue(self.txtComment.text, forKey: "Comment")
            dictVisitorData.setValue(self.txtPosition.text, forKey: "Designation")
            dictVisitorData.setValue(self.txtQrScanner.text, forKey: "ScanIdCardData")
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AdditionalInfoVC") as! AdditionalInfoVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getCompanySizeList() -> NSMutableArray {
        let aryTemp = getDataFromLocal(strEntity: "ComanySizeList", strkey: "size")
        let aryTempOutboxdata = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryTempOutboxdata.add(obj.value(forKey: "size"))
            }
            return (aryTempOutboxdata.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
        }
        return NSMutableArray()
    }
    
    
    
    func getAllVisitorImport() {
        
        let aryEventHistory = getDataFromLocal(strEntity: "EventVisitorList", strkey: "eventvisitorlist")
        let aryTempEventHistory = NSMutableArray()
        if aryEventHistory.count > 0 {
            for j in 0 ..< aryEventHistory.count {
                var obj = NSManagedObject()
                obj = aryEventHistory[j] as! NSManagedObject
                aryTempEventHistory.add(obj.value(forKey: "eventvisitorlist"))
            }
            // print(aryTempEventHistory)
            
        }
        
        let aryfinal = NSMutableArray()
        let strEventId = "\(String(describing: dictData.value(forKey: "EventId")!))"
        
        if aryTempEventHistory.count > 0 {
            for i in 0 ..< aryTempEventHistory.count {
                var obj = NSArray()
                
                let dict = aryTempEventHistory.object(at: i)as! NSDictionary
                let strid = "\(dict.value(forKey: "EventID")!)"
                if strid == strEventId  {
                    obj = dict.value(forKey: "VisitorImportDBList")as! NSArray
                    aryfinal.add(obj)
                    
                }
                
            }
            if aryfinal.count > 0 {
                self.aryForVisitorData = NSMutableArray()
                self.aryForVisitorData = (aryfinal.object(at: 0) as! NSArray).mutableCopy()as! NSMutableArray
            }else{
                //iToast.makeText("Record not found.").show()
                
            }
            
        }
        
        
    }
    
    func FilterDataAccordingVisitorID(Searching: NSString) -> Void {
        
        if aryForVisitorData.count == 0{
             getAllVisitorImport()
        }
     
        txtLeadName.text = ""
        txtPosition.text = ""
        txtCompanyName.text = ""
        txtAddress.text = ""
        txtCity.text = ""
        txtState.text = ""
        txtZipCode.text = ""
        txtCountry.text = ""
        txtContactNumber.text = ""
        txtEmailAddress.text = ""
       // txtQrScanner.text = ""
        txtType.text = ""

        for dict in aryForVisitorData {
            //print(dict)
            let strvisitorID = "\(String(describing: (dict as AnyObject).value(forKey: "VisitorId")!))"
            if (txtSearch.text == strvisitorID){
                self.dictDataVisitorBySearch = NSMutableDictionary()
                self.dictDataVisitorBySearch = (dict as! NSDictionary).mutableCopy()as! NSMutableDictionary
                  print(self.dictDataVisitorBySearch)
                txtType.text = self.dictDataVisitorBySearch.value(forKey: "RegistrationType")as? String
                txtLeadName.text = self.dictDataVisitorBySearch.value(forKey: "FirstName")as? String
                var strAreacode = self.dictDataVisitorBySearch.value(forKey: "PhoneNoAreaCode")as? String
                var strPhoneNo = self.dictDataVisitorBySearch.value(forKey: "PhoneNo")as? String

                
                if strAreacode != nil {
                    
                }else{
                    strAreacode = ""
                }
                
                
                if strPhoneNo != nil {
                    
                }else{
                    strPhoneNo = ""
                }
                
                txtContactNumber.text = "\(String(describing: strAreacode!))\(String(describing: strPhoneNo!))"
                    txtEmailAddress.text = self.dictDataVisitorBySearch.value(forKey: "EmailId")as? String
                txtPosition.text = self.dictDataVisitorBySearch.value(forKey: "Title")as? String
                txtCompanyName.text = self.dictDataVisitorBySearch.value(forKey: "VisitorCompany")as? String
                txtCountry.text = self.dictDataVisitorBySearch.value(forKey: "CompanyCountry")as? String
                txtState.text = self.dictDataVisitorBySearch.value(forKey: "CompanyState")as? String
                txtZipCode.text = self.dictDataVisitorBySearch.value(forKey: "CompanyZipCode")as? String
                txtCity.text = self.dictDataVisitorBySearch.value(forKey: "CompanyCity")as? String
                txtAddress.text = self.dictDataVisitorBySearch.value(forKey: "Address1")as? String
                txtQrScanner.text = ""
                
            }
        }
        if self.dictDataVisitorBySearch.count == 0 {
            iToast.makeText("Record not found.").show()
            txtLeadName.text = ""
            txtPosition.text = ""
            txtCompanyName.text = ""
            txtAddress.text = ""
            txtCity.text = ""
            txtState.text = ""
            txtZipCode.text = ""
            txtCountry.text = ""
            txtContactNumber.text = ""
            txtEmailAddress.text = ""
            //txtQrScanner.text = ""
            txtType.text = ""

        }
        self.view.endEditing(true)
    }
    
    
    
    func FilterDataAccordingVisitorName(Searching: String) -> NSArray {
        if aryForVisitorData.count == 0{
             getAllVisitorImport()
        }
      return aryForVisitorData.filter { (task) -> Bool in
            return "\((task as! NSDictionary).value(forKey: "FirstName")!)".lowercased().contains(Searching.lowercased()) || "\((task as! NSDictionary).value(forKey: "LastName")!)".lowercased().contains(Searching.lowercased()) || "\((task as! NSDictionary).value(forKey: "VisitorCompany")!)".lowercased().contains(Searching.lowercased())
       } as NSArray
        
 
        
    }
    
    
    
    func refreshView()  {
        if(aryForSearchVisitorData.count == 0){
            self.tvSearch.removeFromSuperview()
            scrollView.isScrollEnabled = true
            self.tvSearch.reloadData()

        }else{
            self.tvSearch.frame = CGRect(x: Int(txtLeadName.frame.origin.x), y: 400, width: Int(txtLeadName.frame.width), height: 185)
            self.scrollView.addSubview(self.tvSearch)
            self.tvSearch.delegate = self
            self.tvSearch.dataSource = self
            self.tvSearch.reloadData()
          
            scrollView.isScrollEnabled = false
        }
    }
    
    
    func takePhoto() {
        self.tvSearch.removeFromSuperview()
        scrollView.isScrollEnabled = true
        self.tvSearch.reloadData()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
            
        }else{
            iToast.makeText("Unable to access the Camera.").show()
        }
    }
    
    func resizeImage(_ image: UIImage) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = actualHeight / 1.5
        let maxWidth: Float = actualWidth / 1.5
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        //        //let rect = CGSizeMake(CGFloat(actualWidth),CGFloat(actualHeight))
        //        UIGraphicsBeginImageContext(rect)
        //       // image.draw(in: rect)
        var img = UIGraphicsGetImageFromCurrentImageContext()
        var imageData = UIImageJPEGRepresentation(image,0)!
        UIGraphicsEndImageContext()
        return UIImage(data: imageData)!
        
    }
    
    //MARK:- ---------UIImagePicker Delegate----------
    //MARK:-
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        
        let data:Data = UIImageJPEGRepresentation((info[UIImagePickerControllerOriginalImage] as? UIImage)!,0)!
        // print(data)
        
        
        if strTag == "1" {
            img1.image = UIImage(data: data)!
            btnCross1.isHidden = false
            strImageName1 = "\(WebService().get_currentTime()).jpg"
        }else if (strTag == "2"){
            img2.image = UIImage(data: data)!
            btnCross2.isHidden = false
            strImageName2 = "\(WebService().get_currentTime()).jpg"
            
            
        }else if (strTag == "3"){
            img3.image = UIImage(data: data)!
            btnCross3.isHidden = false
            strImageName3 = "\(WebService().get_currentTime()).jpg"
            
        }else if (strTag == "4"){
            img4.image = UIImage(data: data)!
            btnCross4.isHidden = false
            strImageName4 = "\(WebService().get_currentTime()).jpg"
            
        }
        
    }
    
    //MARK:
    //MARK: -----------TEXT FILED DELEGATE METHOD----------
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtContactNumber)
        {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if((txtContactNumber.text?.count)! > 11) {
                if (string == "")
                {
                    return true;
                }
                return false
            }
            return string == numberFiltered
            
        }
       else if(textField == txtZipCode)
        {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if((txtZipCode.text?.count)! > 7) {
                if (string == "")
                {
                    return true;
                }
                return false
            }
            return string == numberFiltered
        }
        
       else if(textField == txtLeadName){
            aryForSearchVisitorData = NSMutableArray()
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        aryForSearchVisitorData = (self.FilterDataAccordingVisitorName(Searching: txtAfterUpdate as String)).mutableCopy() as! NSMutableArray
        let topOffset = CGPoint(x: 0, y: 230)
        scrollView.setContentOffset(topOffset, animated: true)
            self.refreshView()
        }
       else{
        self.tvSearch.removeFromSuperview()

       }
        //        if(textField == txtCountry || textField == txtState || textField == txtCity || textField == txtLeadName)
        //        {
        //
        //            let characterSet = CharacterSet.letters
        //            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
        //
        //                if (string == " ") {
        //                    return false;
        //                }
        //                return false
        //            }
        //
        //            if ((textField.text?.characters.count)! > 0) {
        //
        //                if ((textField.text?.characters.count)! > 60) {
        //                    if (string == "")
        //                    {
        //                        return true;
        //                    }
        //                    return false;
        //                }
        //                return true;
        //            }
        //            if (string == " ") {
        //                return false;
        //            }
        //        }
        //
        
        
        return true
    }
    func refreshQRBarCodeData(strComment : String) {
        //Changes to call service on scanner did finish
        self.txtQrScanner.text = strComment
        if strComment.range(of:"$") != nil {
            let fullNameArr = strComment.split{$0 == "$"}.map(String.init)
            if fullNameArr.count > 0 {
                let Visitorid: String = fullNameArr[0]
                txtSearch.text = Visitorid
            }
            if fullNameArr.count > 2 {
                let LeadName: String = "\(fullNameArr[1]) \(fullNameArr[2])"
                txtLeadName.text = LeadName
            }
            if fullNameArr.count > 3 {
                let Position: String = fullNameArr[3]
                txtPosition.text = Position
            }
            if fullNameArr.count > 4 {
                let CompanyName: String = fullNameArr[4]
                txtCompanyName.text = CompanyName
            }
            if fullNameArr.count > 6 {
                let Address: String = "\(fullNameArr[5]) \(fullNameArr[6])"
                txtAddress.text = Address
            }
            if fullNameArr.count > 7 {
                let City: String = fullNameArr[7]
                txtCity.text = City
            }
            if fullNameArr.count > 8 {
                let State: String = fullNameArr[8]
                txtState.text = State
            }
            if fullNameArr.count > 9 {
                let Zip: String = fullNameArr[9]
                txtZipCode.text = Zip
            }
            if fullNameArr.count > 10 {
                let Country: String = fullNameArr[10]
                txtCountry.text = Country
            }
            if fullNameArr.count > 11 {
                let Phonenumber: String = fullNameArr[11]
                txtContactNumber.text = Phonenumber
            }
            if fullNameArr.count > 12 {
                let Email: String = fullNameArr[12]
                txtEmailAddress.text = Email
            }
        }else{
            self.txtSearch.text = strComment
        }
    }
}
//MARK:- ----------RefreshLeaddata Delegate Methods----------
//MARK:-

extension AddNewLeadVC: refreshLeaddata {
    func refreshData(audioData: Data) {
        audio_Data = Data()
        audio_Data = audioData
        strVoiceName = "\(WebService().get_currentTime()).mp3"
    }
}
//MARK:- ----------UITableView Delegate Methods----------
//MARK:-
extension AddNewLeadVC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.tvSearch){
            return aryForSearchVisitorData.count
        }else{
            return aryForTv.count
        }
      
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == self.tvSearch){
            self.tvSearch.removeFromSuperview()
            scrollView.isScrollEnabled = true
            self.tvSearch.reloadData()
            self.view.endEditing(true)
            self.dictDataVisitorBySearch = NSMutableDictionary()
            self.dictDataVisitorBySearch = (aryForSearchVisitorData.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary

              print(self.dictDataVisitorBySearch)
            txtType.text = self.dictDataVisitorBySearch.value(forKey: "RegistrationType")as? String
            txtLeadName.text = self.dictDataVisitorBySearch.value(forKey: "FirstName")as? String
            var strAreacode = self.dictDataVisitorBySearch.value(forKey: "PhoneNoAreaCode")as? String
            var strPhoneNo = self.dictDataVisitorBySearch.value(forKey: "PhoneNo")as? String

            
            if strAreacode != nil {
                
            }else{
                strAreacode = ""
            }
            
            
            if strPhoneNo != nil {
                
            }else{
                strPhoneNo = ""
            }
            
            txtContactNumber.text = "\(String(describing: strAreacode!))\(String(describing: strPhoneNo!))"
                txtEmailAddress.text = self.dictDataVisitorBySearch.value(forKey: "EmailId")as? String
            txtPosition.text = self.dictDataVisitorBySearch.value(forKey: "Title")as? String
            txtCompanyName.text = self.dictDataVisitorBySearch.value(forKey: "VisitorCompany")as? String
            txtCountry.text = self.dictDataVisitorBySearch.value(forKey: "CompanyCountry")as? String
            txtState.text = self.dictDataVisitorBySearch.value(forKey: "CompanyState")as? String
            txtZipCode.text = self.dictDataVisitorBySearch.value(forKey: "CompanyZipCode")as? String
            txtCity.text = self.dictDataVisitorBySearch.value(forKey: "CompanyCity")as? String
            txtAddress.text = self.dictDataVisitorBySearch.value(forKey: "Address1")as? String
            txtQrScanner.text = ""
        }else{
            if tv.tag == 20 {
                let strCompanySize = aryForTv[indexPath.row]
                txtType.text = strCompanySize as? String
            }
           else{
                let dictTemp = aryForTv.object(at: indexPath.row)as? NSDictionary
                txtCompanySize.text = dictTemp?.value(forKey: "CompanySize")as? String
                txtCompanySize.tag = (dictTemp?.value(forKey: "CompanySizeId")as? Int)!
            }
            btnTransprant.isHidden = true
            viewShowlist.isHidden = true
        }
        
        
   
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == self.tvSearch){
            let cell = tvSearch.dequeueReusableCell(withIdentifier: "Search", for: indexPath as IndexPath) as! CommonTBLCell

            let dict = aryForSearchVisitorData.object(at: indexPath.row)as! NSDictionary
            cell.provider_lblTitle.text = "\(dict.value(forKey: "FirstName") ?? "")" + " \(dict.value(forKey: "LastName") ?? "")" + " (\(dict.value(forKey: "VisitorCompany") ?? ""))"
            return cell

        }else{
            let cell = tv.dequeueReusableCell(withIdentifier: "companySize", for: indexPath as IndexPath) as! CommonTBLCell

            if tv.tag == 20 {
                let strCompanySize = aryForTv[indexPath.row]
                cell.company_lblList.text = strCompanySize as? String
                if txtType.text == cell.company_lblList.text {
                    cell.company_checkicon.isHidden = false
                }else{
                    cell.company_checkicon.isHidden = true

                }
                
            }
          
            else{
                let dict = aryForTv.object(at: indexPath.row)as! NSDictionary
                let strCompanySize = dict.value(forKey: "CompanySize")as? String
                cell.company_lblList.text = strCompanySize!
                cell.company_checkicon.isHidden = true

            }
            return cell

        }
        
  
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == self.tvSearch){
            return UITableViewAutomaticDimension

        }
        else{
            return 50

        }
       
    }
    
}
extension UIImage {
    var uncompressedPNGData: Data?      { return UIImagePNGRepresentation(self)        }
    var highestQualityJPEGNSData: Data? { return UIImageJPEGRepresentation(self, 1.0)  }
    var highQualityJPEGNSData: Data?    { return UIImageJPEGRepresentation(self, 0.75) }
    var mediumQualityJPEGNSData: Data?  { return UIImageJPEGRepresentation(self, 0.5)  }
    var lowQualityJPEGNSData: Data?     { return UIImageJPEGRepresentation(self, 0.25) }
    var lowestQualityJPEGNSData:Data?   { return UIImageJPEGRepresentation(self, 0.0)  }
}
