//
//  EventSearchVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/4/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class EventSearchVC: UIViewController {
    
    var aryForTv = NSMutableArray()
    var aryListData = NSMutableArray()

    var dictData = NSMutableDictionary()
    var dict_VisitorData = NSMutableDictionary()

    var strCome = String()
    var tagcount = 0
    
    var apiCallingTag = Int()
    var apiCallForTag = Int()
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    
    //MARK:- ----------LIFE CYCLE ----------
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCallingTag = 0
        apiCallForTag = 0
        print(dictData)
        tv.tableFooterView = UIView()

        lblTitle.text = dictData.value(forKey: "Event")as? String
        tv.estimatedRowHeight = 120.0
        if strCome == "history" {
            if checkInternetConnection() == true {
                CallEventVisitor_API()
            }else{
                let strEventId = "\(String(describing: dictData.value(forKey: "EventId")!))"
                getEventHistopryFromlocal(strEventID: strEventId)
            }
            
            
        }else{
            //--- Get OUTBOXDATA
            getOutBoxData()
            lblTitle.text = "Outbox"

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    //MARK:- ----------All IBACTION ----------
    //MARK:-
    @IBAction func actionOnBack(_ sender: Any) {
        if strCome == "history" ||  strCome == "outbox"{
            navigationController?.popViewController(animated: true)

        }else{
            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }
    
    

    }
    
    func checkCall(tag : Int) {
        if checkInternetConnection() == true {
            if aryForTv.count > 0{
            let dict = aryForTv.object(at: tag)
            self.sendOuboxData(dict: dict as! NSDictionary)
            }
        }else{
            
        }
    }
    
    func sendOuboxData(dict : NSDictionary)  {
       // print(dict)
        var dictemp = NSMutableDictionary()
        dictemp = dict.mutableCopy()as! NSMutableDictionary
       
        audio_Data = dictemp.value(forKey: "audionamedata")as! Data
        
        strImageName1 = dictemp.value(forKey: "img1Name")as! String
        strImageName2 = dictemp.value(forKey: "img2Name")as! String
        strImageName3 = dictemp.value(forKey: "img3Name")as! String
        strImageName4 = dictemp.value(forKey: "img4Name")as! String
        strVoiceName = dictemp.value(forKey: "audioname")as! String
        
        
        image1 = getImagefromDirectory(strname : strImageName1)
        image2 = getImagefromDirectory(strname : strImageName2)
        image3 = getImagefromDirectory(strname : strImageName3)
        image4 = getImagefromDirectory(strname : strImageName4)

        
        
//        dictemp.removeObject(forKey: "img1")
//        dictemp.removeObject(forKey: "img2")
//        dictemp.removeObject(forKey: "img3")
//        dictemp.removeObject(forKey: "img4")

        dictemp.removeObject(forKey: "img1Name")
        dictemp.removeObject(forKey: "img2Name")
        dictemp.removeObject(forKey: "img3Name")
        dictemp.removeObject(forKey: "img4Name")
        
        dictemp.removeObject(forKey: "audionamedata")
        dictemp.removeObject(forKey: "audioname")
         dict_VisitorData = NSMutableDictionary()
        dict_VisitorData  = dictemp
       // print(dict)
        if image1 != UIImage(named:"defult_img") && strImageName1.characters.count > 0 {
            apiCallingTag =  apiCallingTag + 1
        }
        if image2 != UIImage(named:"defult_img") && strImageName2.characters.count > 0 {
            apiCallingTag = apiCallingTag + 1
        }
        if image3 != UIImage(named:"defult_img") && strImageName3.characters.count > 0 {
            apiCallingTag = apiCallingTag + 1
        }
        if image4 != UIImage(named:"defult_img") && strImageName4.characters.count > 0{
            apiCallingTag = apiCallingTag + 1
        }
        
        if image1 == UIImage(named:"defult_img") || strImageName1 == "" {
            
        }else{
            callUploadImage(imageData: image1, strname: strImageName1)
        }
        if image2 == UIImage(named:"defult_img") || strImageName2 == "" {
        }else{
            callUploadImage(imageData: image2, strname: strImageName2)
            
        }
        
        if image3 == UIImage(named:"defult_img") || strImageName3 == "" {
            
        }else{
            callUploadImage(imageData: image3, strname: strImageName3)
        }
        if image4 == UIImage(named:"defult_img") || strImageName4 == ""{
        }else{
            callUploadImage(imageData: image4, strname: strImageName4)
            
        }
        if (audio_Data.count > 0 ) {
            callUploadAudio(audioData: audio_Data, strname: strVoiceName)
        }
        if apiCallForTag == apiCallingTag {
           // CallAdd_NEWLead()
        }
         CallAdd_NEWLead()
    }
    
    //MARK:
    //MARK: -----------ALL API CALLING---------
    
    
    
    func getEventHistopryFromlocal(strEventID : String)  {
        DispatchQueue.main.async {
            
        let aryEventHistory = getDataFromLocal(strEntity: "EventHistory", strkey: "history")
        let aryTempEventHistory = NSMutableArray()
        if aryEventHistory.count > 0 {
            for j in 0 ..< aryEventHistory.count {
                var obj = NSManagedObject()
                obj = aryEventHistory[j] as! NSManagedObject
                aryTempEventHistory.add(obj.value(forKey: "history"))
            }
            // print(aryTempEventHistory)
            
        }
        let aryfinal = NSMutableArray()
        if aryTempEventHistory.count > 0 {
            for i in 0 ..< aryTempEventHistory.count {
                var obj = NSArray()
                let strid = "\(String(describing: (aryTempEventHistory[i] as! NSDictionary).value(forKey: "EventId")!))"
                if strid == strEventID  {
                    obj = (aryTempEventHistory[i] as! NSDictionary).value(forKey: "VisitorList") as! NSArray
                    aryfinal.add(obj)
                    
                }
                
            }
            if aryfinal.count > 0 {
                self.aryForTv = NSMutableArray()
                self.aryForTv = (aryfinal.object(at: 0) as! NSArray).mutableCopy()as! NSMutableArray
                self.aryListData = NSMutableArray()
                self.aryListData = self.aryForTv
                self.tv.reloadData()
                self.tv.isHidden = false

            }else{
                iToast.makeText("Event not found.").show()
                 self.tv.isHidden = true
            }
            
        }
        }
    }
    

    
    
    
    
    
    func deletdataFromlocal() {
        let aryTemp = getDataFromLocal(strEntity: "OutBox", strkey: "outbox")
        
        if aryTemp.count > 0 {
            let obj = aryTemp.object(at: 0) as! NSManagedObject
            let context = AppDelegate.getContext()
            
            //save the object
            do {
                context.delete(obj)
                try context.save()
                self.getOutBoxData()
            } catch let error as NSError  {
             
            } catch {
                
            }

        
        }
        
        
      
    }
    
    func getOutBoxData() {
        
        print(strCome)
        DispatchQueue.main.async {

        let aryTemp = getDataFromLocal(strEntity: "OutBox", strkey: "outbox")
        
        let aryTempOutboxdata = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryTempOutboxdata.add(obj.value(forKey: "outbox"))
            }
            //print(aryTempOutboxdata)
            self.aryForTv = NSMutableArray()
            self.aryForTv = aryTempOutboxdata
            self.aryListData = NSMutableArray()
            self.aryListData = aryTempOutboxdata
            if self.strCome == "history" || self.strCome == "outbox"  {
                 self.tv.reloadData()
                 self.tv.tableFooterView = UIView()
                if self.aryForTv.count > 0 {
                    self.tv.isHidden = false
                }else{
                    self.tv.isHidden = true
                }

            }
            
            self.checkCall(tag: 0)
        }else{
            if self.strCome == "history" || self.strCome == "outbox"  {

                self.tv.isHidden = true
            }
            
            
        }
        }
    }
    
    func CallEventVisitor_API() {
        
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictData.value(forKey: "CompanyId")!))"
            let strEventId = "\(String(describing: dictData.value(forKey: "EventId")!))"
            dict.setValue(strEventId, forKey: "EventId")
            dict.setValue(strCompanyID, forKey: "CompanyId")
            WebService.callAPIBYGET(parameter: dict, url: API_EVENT_VISITOR, OnResultBlock: {  (result, status) in
               // print(result);
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "EventCompanyVisitorList")as! NSArray
                        
                        
                        self.aryForTv = NSMutableArray()
                        self.aryForTv = aryTemp.mutableCopy()as! NSMutableArray
                        self.aryListData = NSMutableArray()
                        self.aryListData = aryTemp.mutableCopy()as! NSMutableArray
                        self.tv.reloadData()
                        if self.aryForTv.count > 0 {
                            self.tv.isHidden = false
                        }else{
                            self.tv.isHidden = true
                        }
                        
                        let dict = NSMutableDictionary()
                        dict.setValue(strEventId, forKey: "EventId")
                        dict.setValue(aryTemp, forKey: "VisitorList")
                        var strTag = 0
                        //--------ForcheckEvent are exist or not
                        let aryEventHistory = getDataFromLocal(strEntity: "EventHistory", strkey: "history")
                        deleteAllRecords(strEntity: "EventHistory")
                        if aryEventHistory.count > 0 {
                            for j in 0 ..< aryEventHistory.count {
                                var obj = NSManagedObject()
                                let aryTempEventHistory = NSMutableArray()
                                obj = aryEventHistory[j] as! NSManagedObject
                                aryTempEventHistory.add(obj.value(forKey: "history"))
                                let strid = "\(String(describing: (aryTempEventHistory[0] as! NSDictionary).value(forKey: "EventId")!))"
                                if strid == strEventId  {
                                    saveDataInLocalDict(strEntity: "EventHistory", strKey: "history", data: dict)
                                    strTag = 1
                                }else{
                                    saveDataInLocalDict(strEntity: "EventHistory", strKey: "history", data: obj.value(forKey: "history") as! NSMutableDictionary)
                                }
                            }
                            if strTag == 0{
                                saveDataInLocalDict(strEntity: "EventHistory", strKey: "history", data: dict)
                                
                            }
                        }
                            //---------FirstTime---------
                            
                        else{
                            saveDataInLocalDict(strEntity: "EventHistory", strKey: "history", data: dict)
                        }

                        KRProgressHUD.dismiss()
                    }else{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                        if self.aryForTv.count > 0 {
                            self.tv.isHidden = false
                        }else{
                            self.tv.isHidden = true
                        }
                        KRProgressHUD.dismiss()

                    }
                }
                else{
                    iToast.makeText("The request timed out.").show()
                    KRProgressHUD.dismiss()

                }
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
            KRProgressHUD.dismiss()

        }
    }
    
    //--For SendOutbox Data when net On
    
    func callUploadAudio(audioData : Data , strname : String)  {
        if checkInternetConnection() == true {
            if self.strCome == "Outbox"  {
                
                KRProgressHUD.show(message: "Loading...")
            }
            WebService.callAPIWithAudio(parameter:  NSDictionary(), url: BaseURL_AudioUpload, audioData: audioData, filename: strname, OnResultBlock: {  (result, status) in
                //print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let strstatus = result.value(forKey: "message")as! String
                    if strstatus == "SUCCESS"{
                        
                    }else{
                        let strstatus = result.value(forKey: "message")as! String
                        iToast.makeText(strstatus).show()
                    }
                }
                else{
                    
                    
                }
                
            })
            
            
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    
    func callUploadImage(imageData : UIImage , strname : String)  {
        if checkInternetConnection() == true {
            if self.strCome == "Outbox"  {

            KRProgressHUD.show(message: "Loading...")
            }
            WebService.callAPIWithImageOnlyOne(parameter: NSDictionary(), url: BaseURL_ImageUpload, image: imageData, imageName: strname, OnResultBlock: {  (result, status) in
               // print(result);
                if status == "Suceess"{
                    let strstatus = result.value(forKey: "message")as! String
                    if strstatus == "SUCCESS"{
                        clearTempFolder(strname: strname)
                        self.apiCallForTag = self.apiCallForTag + 1
                        if self.apiCallForTag == self.apiCallingTag {
                           // self.CallAdd_NEWLead()
                        }
                    }else{
                        KRProgressHUD.dismiss()
                        let strstatus = result.value(forKey: "message")as! String
                        iToast.makeText(strstatus).show()
                    }
                }
                else{
                    KRProgressHUD.dismiss()
                }
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    
    
    func CallAdd_NEWLead() {
        if checkInternetConnection() == true {
            if self.strCome == "outbox"  {
                KRProgressHUD.show(message: "Loading...")
            }
            WebService.callAPIBYPOST(parameter: dict_VisitorData, url: API_ADDCompanyVisitor, OnResultBlock: {  (result, status) in
                print(result);
              //  KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let message =  dict.value(forKey: "Success")as! String
                   
                        if self.aryForTv.count == 1{

                        KRProgressHUD.dismiss()
                         deleteAllRecords(strEntity: "OutBox")
                            if self.strCome == "outbox"  {
                                iToast.makeText(message).show()
                                let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                                appDelegate.window?.rootViewController = mainVcIntial
                            }
                      
                        }else{
                            self.deletdataFromlocal()
                        }
                    }else{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                        let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                    }
                }
                else{
                    
                    
                }
                
            })
            
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    

}
//MARK:- ----------UITableView Delegate Methods----------
//MARK:-
extension EventSearchVC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryForTv.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if strCome == "history" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailVC
            let dictTemp = aryForTv.object(at: indexPath.row)as? NSDictionary
            vc.dictData = dictTemp?.mutableCopy()as! NSMutableDictionary
            vc.strCome = "history"
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailVC
//            let dictTemp = aryForTv.object(at: indexPath.row)as? NSDictionary
//            vc.dictData = dictTemp?.mutableCopy()as! NSMutableDictionary
//            vc.strCome = "outbox"
//            self.navigationController?.pushViewController(vc, animated: true)
        }
   }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        
        let cell = tv.dequeueReusableCell(withIdentifier: "EventSearchCell", for: indexPath as IndexPath) as! CommonTBLCell
        let dict = aryForTv.object(at: indexPath.row)as! NSDictionary

        
        let strname = dict.value(forKey: "Name")as? String
        let strDesignation = dict.value(forKey: "Designation")as? String
        let strEmailId = dict.value(forKey: "EmailId")as? String
        let strContactNo = dict.value(forKey: "ContactNo")as? String
        let strProfileImage = dict.value(forKey: "ProfileImage")as? String

        //--Check null conditions
        
        if strname != nil {
            cell.eventS_lblName.text = strname!
            if strname?.characters.count == 0 {
                cell.eventS_lblName.text = " "
            }
        }else{
            cell.eventS_lblName.text = " "
        }
        if strDesignation != nil {
            cell.eventS_lblRole.text = strDesignation!
            if strDesignation?.characters.count == 0 {
                cell.eventS_lblRole.text = " "
            }

        }else{
            cell.eventS_lblRole.text = " "
        }
        if strContactNo != nil {
            cell.eventS_lblContactNumber.text = strContactNo!
            if strContactNo?.characters.count == 0 {
                cell.eventS_lblContactNumber.text = " "
            }
        }else{
            cell.eventS_lblContactNumber.text = " "
 
        }
        if strEmailId != nil {
            cell.eventS_Email.text = strEmailId!
            if strEmailId?.characters.count == 0 {
                cell.eventS_Email.text = " "
            }
        }else{
            cell.eventS_Email.text = " "
        }
      
        if strCome != "history" {
            let strProfileImagename = dict.value(forKey: "img1Name")as? String
            if strProfileImagename == ""{
                cell.eventS_img.image = UIImage(named: "defult_img")

            }else{
                cell.eventS_img.image = getImagefromDirectory(strname : strProfileImagename!)
            }
        }
        else{
            let strUrlImage = "\(BaseURL_Image)\(String(describing: strProfileImage!))"
            
            
            cell.eventS_img.setImageWith(URL(string: strUrlImage), placeholderImage: UIImage(named: "defult_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)

        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
//MARK: - UITextFieldDelegate
//MARK: -

extension EventSearchVC: UITextFieldDelegate  {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        print("\(txtAfterUpdate)")
        return true
    }
    
    func searchAutocomplete(Searching: NSString) -> Void {
        let resultPredicate = NSPredicate(format: "Name contains[c] %@ OR Name contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (aryListData).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryForTv = NSMutableArray()
            self.aryForTv = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tv.reloadData()
        }
        else{
            self.aryForTv = NSMutableArray()
            self.aryForTv = aryListData.mutableCopy() as! NSMutableArray
            self.tv.reloadData()
            self.view.endEditing(true)
            txtSearch.text = ""
        }
}
}
