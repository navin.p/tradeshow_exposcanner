//
//  ForgotPasswordVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/3/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    var isSubmitButtonPressed = false
    var showingTitleInProgress = false
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!

    
    //MARK:
    //MARK: -----------Life Cycle----------
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.txtEmail.delegate = self
        self.btnSubmit.layer.borderWidth = 1.0
        self.btnSubmit.layer.borderColor = UIColor.lightGray.cgColor
        self.btnSubmit.layer.cornerRadius = 8.0

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func actionOnSubmit(_ sender: Any) {
        self.isSubmitButtonPressed = true
        
        if !self.txtEmail.hasText {
            self.showingTitleInProgress = true
            self.txtEmail.setTitleVisible(true, animated: true, animationCompletion: self.showingTitleInAnimationComplete)
            self.txtEmail.isHighlighted = true
            iToast.makeText("Please Enter Email Id.").show()
            
        }else if (isValidEmail(str: txtEmail.text)) != true{
            self.showingTitleInProgress = true
            self.txtEmail.setTitleVisible(true, animated: true, animationCompletion: self.showingTitleInAnimationComplete)
            self.txtEmail.isHighlighted = true
            iToast.makeText("Please Enter Valid Email Id.").show()
        }
        else{
            if checkInternetConnection() == true {
                CallForgotPasswordAPI()
            }else{
                iToast.makeText("Please check internet connection.").show()
            }
        }
        self.isSubmitButtonPressed = false
        if(!self.showingTitleInProgress) {
            self.hideTitleVisibleFromFields()
        }
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
      //MARK:
    //MARK: -----------ALL PRIVATE FUNCTION----------
    
    func showingTitleInAnimationComplete() {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.20) {
            self.showingTitleInProgress = false
            if(!self.isSubmitButtonPressed) {
                self.hideTitleVisibleFromFields()
            }
        }
    }
    
    func hideTitleVisibleFromFields() {
        
        self.txtEmail.setTitleVisible(false, animated: true)
        self.txtEmail.isHighlighted = false
                
    }
    
    
    func isValidEmail(str:String?) -> Bool {
        let emailRegEx = "[A-Za-z0-9._]+@[A-Za-z0-9]+\\.[A-Za-z]{2,4}";
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: str)
        return result
    }
    
    //MARK:
    //MARK: -----------ALL API CALLING---------
    func CallForgotPasswordAPI() {
        
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            dict.setValue(txtEmail.text, forKey: "EmailId")
            WebService.callAPIBYGET(parameter: dict, url: API_FORGOTPASS, OnResultBlock: {  (result, status) in
                print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                        self.navigationController?.popViewController(animated: true)

                        
                    }else{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                    }
                }
                else{
                    
                    
                }
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
 
}

//MARK:
//MARK: -----------TEXT FILED DELEGATE METHOD----------

extension ForgotPasswordVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //
        textField.resignFirstResponder()
        
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtEmail)
        {
            if ((txtEmail.text?.characters.count)! > 0) {
                
                if ((txtEmail.text?.characters.count)! > 30) {
                    if (string == "")
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            if (string == " ") {
                return false;
            }
        }
        return true
    }
    

}
