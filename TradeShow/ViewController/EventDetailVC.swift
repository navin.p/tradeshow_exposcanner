//
//  EventDetailVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/4/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//
import UIKit
import AVKit
import AVFoundation


class EventDetailVC: UIViewController ,AVAudioPlayerDelegate,AVPlayerViewControllerDelegate,UIGestureRecognizerDelegate {
    
    var aryForTv = NSMutableArray()
    var dictData = NSMutableDictionary()
    var player : AVPlayer?
    var strCome = String()
    var playerController = AVPlayerViewController()
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tv: UITableView!
    //MARK:- ----------LIFE CYCLE----------
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        tv.estimatedRowHeight = 558.0
        print(dictData)
        self.lblTitle.text = dictData.value(forKey: "Name")as? String
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    func show_FullStoryImage(gesture : UITapGestureRecognizer){
        
       // let tappedImage = gesture.view as! UIImageView
        
      //  UUImageAvatarBrowser.showImage(forZoomView: self, withImage: tappedImage)
        
    }
    
    //MARK:- ----------ALL IBACTION----------
    //MARK:-
    
    @IBAction func actionOnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
}
//MARK:- ----------UITableView Delegate Methods----------
//MARK:-
extension EventDetailVC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tv.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath as IndexPath) as! CommonTBLCell
        cell.DETAIL_VOICEVIEW.layer.borderWidth = 1.0
        cell.DETAIL_VOICEVIEW.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
        if strCome == "outbox" {
            
            let strVoiceNotename = dictData.value(forKey: "audioname")as? String
            cell.detail_Play.tag = indexPath.row
            cell.detail_Play.addTarget(self, action:#selector(playAudio(sender:)), for: .touchUpInside)
            if strVoiceNotename != nil {
                cell.detail_lblNofile.text = strVoiceNotename!
                cell.detail_Play.isEnabled = true
                if strVoiceNotename?.characters.count == 0 {
                    cell.detail_lblNofile.text = "No file"
                    cell.detail_Play.isEnabled = false
                }
            }else{
                cell.detail_lblNofile.text = "No file"
                cell.detail_Play.isEnabled = false
            }

            let strVisitorId = dictData.value(forKey: "VisitorIdNo")as? String
            let strContactNo = dictData.value(forKey: "ContactNo")as? String
            let strEmailId = dictData.value(forKey: "EmailId")as? String
            let strDesignation = dictData.value(forKey: "Designation")as? String
            
            let strCompanyName = dictData.value(forKey: "CompanyName")as? String
            let strCompanySize = dictData.value(forKey: "CompanySize")as? String
            let strAddress = dictData.value(forKey: "Address")as? String
            let strComment = dictData.value(forKey: "Comment")as? String
            
            let strProfileimg = dictData.value(forKey: "img1")as! UIImage
            let strCaptureImage = dictData.value(forKey: "img2")as! UIImage
            
            let strCardBackImage = dictData.value(forKey: "img3")as! UIImage
            
            let strCardFrontImage = dictData.value(forKey: "img4")as! UIImage
            
            //--Check null conditions
            
            if strVisitorId != nil {
                cell.detail_lblVisitorid.text = strVisitorId!
                if strVisitorId?.characters.count == 0 {
                    cell.detail_lblVisitorid.text = " "
                }
            }else{
                cell.detail_lblVisitorid.text = " "
            }
            
            if strContactNo != nil {
                cell.detail_lblContactNumber.text = strContactNo!
                if strContactNo?.characters.count == 0 {
                    cell.detail_lblContactNumber.text = " "
                }
            }else{
                cell.detail_lblContactNumber.text = " "
            }
            
            if strEmailId != nil {
                cell.detail_lblEmail.text = strEmailId!
                if strEmailId?.characters.count == 0 {
                    cell.detail_lblEmail.text = " "
                }
            }else{
                cell.detail_lblEmail.text = " "
            }
            
            if strDesignation != nil {
                cell.detail_lblPossition.text = strDesignation!
                if strDesignation?.characters.count == 0 {
                    cell.detail_lblPossition.text = " "
                }
            }else{
                cell.detail_lblPossition.text = " "
            }
            if strCompanyName != nil {
                cell.detail_lblCompanyName.text = strCompanyName!
                if strCompanyName?.characters.count == 0 {
                    cell.detail_lblCompanyName.text = " "
                }
            }else{
                cell.detail_lblCompanyName.text = " "
            }
            
            if strCompanySize != nil {
                cell.detail_lblCompanySize.text = strCompanySize!
                if strCompanySize?.characters.count == 0 {
                    cell.detail_lblCompanySize.text = " "
                }
            }else{
                cell.detail_lblCompanySize.text = " "
            }
            
            if strAddress != nil {
                cell.detail_lblAddressServices.text = strAddress!
                if strAddress?.characters.count == 0 {
                    cell.detail_lblAddressServices.text = " "
                }
            }else{
                cell.detail_lblAddressServices.text = " "
            }
            
            
            if strComment != nil {
                cell.detail_lblComment.text = strComment!
                if strComment?.characters.count == 0 {
                    cell.detail_lblComment.text = " "
                }
            }else{
                cell.detail_lblComment.text = " "
            }
            cell.detail_lblNofile.text = "No file"
            cell.detail_Play.isEnabled = false
            cell.detail_Img1.image = strProfileimg
            cell.detail_img2.image = strCaptureImage
            cell.detail_img3.image = strCardFrontImage
            cell.detail_img4.image = strCardBackImage
       
            
        }else{
            let strVisitorId = dictData.value(forKey: "VisitorIdNo")as? String
            let strContactNo = dictData.value(forKey: "ContactNo")as? String
            let strEmailId = dictData.value(forKey: "EmailId")as? String
            let strDesignation = dictData.value(forKey: "Designation")as? String
            let strCompanyName = dictData.value(forKey: "CompanyName")as? String
            let strCompanySize = dictData.value(forKey: "CompanySize")as? String
            let strAddress = dictData.value(forKey: "Address")as? String
            let strComment = dictData.value(forKey: "Comment")as? String
            
            let strProfileimg = dictData.value(forKey: "ProfileImage")as? String
            let strCaptureImage = dictData.value(forKey: "CaptureImage")as? String
            let strCardBackImage = dictData.value(forKey: "CardBackImage")as? String
            let strCardFrontImage = dictData.value(forKey: "CardFrontImage")as? String
            let strVoiceNote = dictData.value(forKey: "VoiceNote")as? String
            
            //--Check null conditions
            
            if strVisitorId != nil {
                cell.detail_lblVisitorid.text = strVisitorId!
                if strVisitorId?.characters.count == 0 {
                    cell.detail_lblVisitorid.text = " "
                }
            }else{
                cell.detail_lblVisitorid.text = " "
            }
            
            if strContactNo != nil {
                cell.detail_lblContactNumber.text = strContactNo!
                if strContactNo?.characters.count == 0 {
                    cell.detail_lblContactNumber.text = " "
                }
            }else{
                cell.detail_lblContactNumber.text = " "
            }
            
            if strEmailId != nil {
                cell.detail_lblEmail.text = strEmailId!
                if strEmailId?.characters.count == 0 {
                    cell.detail_lblEmail.text = " "
                }
            }else{
                cell.detail_lblEmail.text = " "
            }
            
            if strDesignation != nil {
                cell.detail_lblPossition.text = strDesignation!
                if strDesignation?.characters.count == 0 {
                    cell.detail_lblPossition.text = " "
                }
            }else{
                cell.detail_lblPossition.text = " "
            }
            if strCompanyName != nil {
                cell.detail_lblCompanyName.text = strCompanyName!
                if strCompanyName?.characters.count == 0 {
                    cell.detail_lblCompanyName.text = " "
                }
            }else{
                cell.detail_lblCompanyName.text = " "
            }
            
            if strCompanySize != nil {
                cell.detail_lblCompanySize.text = strCompanySize!
                if strCompanySize?.characters.count == 0 {
                    cell.detail_lblCompanySize.text = " "
                }
            }else{
                cell.detail_lblCompanySize.text = " "
            }
            
            if strAddress != nil {
                cell.detail_lblAddressServices.text = strAddress!
                if strAddress?.characters.count == 0 {
                    cell.detail_lblAddressServices.text = " "
                }
            }else{
                cell.detail_lblAddressServices.text = " "
            }
            
            
            if strComment != nil {
                cell.detail_lblComment.text = strComment!
                if strComment?.characters.count == 0 {
                    cell.detail_lblComment.text = " "
                }
            }else{
                cell.detail_lblComment.text = " "
            }
            
            if strVoiceNote != nil {
                cell.detail_lblNofile.text = strVoiceNote!
                cell.detail_Play.isEnabled = true
                if strVoiceNote?.characters.count == 0 {
                    cell.detail_lblNofile.text = "No file"
                    cell.detail_Play.isEnabled = false
                    
                }
            }else{
                cell.detail_lblNofile.text = "No file"
                cell.detail_Play.isEnabled = false
            }
            
            
            
            let strUrlImage = "\(BaseURL_Image)\(String(describing: strProfileimg!))"
            cell.detail_Img1.setImageWith(URL(string: strUrlImage), placeholderImage: UIImage(named: "defult_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
            
            let strUrlImage1 = "\(BaseURL_Image)\(String(describing: strCaptureImage!))"
            cell.detail_img2.setImageWith(URL(string: strUrlImage1), placeholderImage: UIImage(named: "defult_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
            
            let strUrlImage2 = "\(BaseURL_Image)\(String(describing: strCardBackImage!))"
            cell.detail_img4.setImageWith(URL(string: strUrlImage2), placeholderImage: UIImage(named: "defult_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
            
            let strUrlImage3 = "\(BaseURL_Image)\(String(describing: strCardFrontImage!))"
            cell.detail_img3.setImageWith(URL(string: strUrlImage3), placeholderImage: UIImage(named: "defult_img"), options:.init(rawValue: 0), usingActivityIndicatorStyle: .gray)
            cell.detail_Play.tag = indexPath.row
            cell.detail_Play.addTarget(self, action:#selector(playAudio(sender:)), for: .touchUpInside)
            
      
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
     @objc func playAudio(sender : UIButton)
    {
        let strVoiceNote = dictData.value(forKey: "VoiceNote")as? String
        let url = NSURL.init(string: "\(BaseURL_Image)\(String(describing: strVoiceNote!))")
        _ = AVPlayer()
        let player = AVPlayer(url:url! as URL)
        playerController = AVPlayerViewController()
        
        NotificationCenter.default.addObserver(self, selector: #selector(EventDetailVC.didfinishplaying(note:)),name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
        playerController.player = player
        
        playerController.allowsPictureInPicturePlayback = true
        
        playerController.delegate = self
        
        playerController.player?.play()
        
        self.present(playerController,animated:true,completion:nil)
        
        
        
        //       // let videoURL = URL(string: "\(BaseURL_Image)\(strVoiceNote!)")
        //        let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        //        let player = AVPlayer(url: videoURL!)
        //        let playerViewController = AVPlayerViewController()
        //        playerViewController.player = player
        //        self.present(playerViewController, animated: true) {
        //            playerViewController.player!.play()
        //        }
    }
    @objc func didfinishplaying(note : NSNotification)
    {
        playerController.dismiss(animated: true,completion: nil)
        
    }
}
