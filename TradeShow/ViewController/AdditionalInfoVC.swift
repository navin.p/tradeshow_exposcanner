//
//  AdditionalInfoVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/5/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData
class AdditionalInfoVC: UIViewController ,UITableViewDataSource , UITableViewDelegate , UITextFieldDelegate {

   
    //MARK:- ----------IBOutlet---------
    //MARK:-
    
    //appoimentview--
    
    @IBOutlet weak var btnAppoimentClose: UIButton!
    
    @IBOutlet weak var btnAppoimentOK: UIButton!
    
    @IBOutlet weak var btnAppoimentNoAppoiment: UIButton!
    
    @IBOutlet weak var viewDateTimePicker: UIView!
    @IBOutlet weak var viewAppoiment: CardView!
    
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var tvForCompanyMemberlist: UITableView!

    @IBOutlet weak var txtEmploye: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAppoimentTime: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAppoimentDate: SkyFloatingLabelTextField!
    //----//
    
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnSaveWithGreeting: UIButton!
    @IBOutlet weak var tv: UITableView!
    
    @IBOutlet weak var viewTblSelection: UIView!
    @IBOutlet weak var lblTblTitle: UILabel!
    @IBOutlet weak var tvForProvider: UITableView!
    @IBOutlet weak var btnTransprant: UIButton!

    var apiCallingTag = Int()
    var apiCallForTag = Int()

    var arraySection = NSMutableArray()
    var arrayForProvider = NSMutableArray()
    
    var arryForTempSend = NSMutableArray()
    var arryForSelectedDataForCurrentProvider = NSMutableArray()
    var arryForSelectedDataForConsideringProvider = NSMutableArray()

    var int_expandedSections : NSInteger?
    var dictUserData = NSDictionary()
    let aryIntrested = NSMutableArray()
    
    var aryCompantMemberlist = NSMutableArray()
    
    var tagforCheck = Int()
    var strselected = String()
    
    //MARK:- ----------Life Cycle---------
    //MARK:-
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //--- Get UserData
        aryIntrested.add("Low")
        aryIntrested.add("Medium")
        aryIntrested.add("High")
        let aryTemp = getDataFromLocal(strEntity: "LogInData", strkey: "logindata")
        
        let aryTempdata = NSMutableArray()
        if aryTemp.count > 0 {
            
            
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryTempdata.add(obj.value(forKey: "logindata"))
            }
          //  print(aryTempdata)
            dictUserData = NSDictionary()
            dictUserData = aryTempdata.object(at: 0)as! NSDictionary
        }
        
        tagforCheck = 1
        apiCallingTag = 0
        apiCallForTag = 0
        btnTransprant.isHidden = true
        viewTblSelection.isHidden = true
        viewDateTimePicker.isHidden = true
        viewAppoiment.isHidden = true
        picker.minimumDate = Date()

               int_expandedSections = -1
        self.tv?.rowHeight = UITableViewAutomaticDimension
        self.tv?.estimatedRowHeight = 88
        self.tv.tableFooterView = UIView()
        tvForCompanyMemberlist.tableFooterView = UIView()
        tvForProvider.tableFooterView = UIView()
        btnTransprant.addTarget(self, action: #selector(self.hidePopUP(sender:)), for: .touchUpInside)
           
        if checkInternetConnection() == true {
            CallCategoryAPI()

        }else{
            
            let ary = getDataFromLocal(strEntity: "CategoryList", strkey: "categoryList")
            let aryTempdata = NSMutableArray()
            if ary.count > 0 {
                for j in 0 ..< ary.count {
                    var obj = NSManagedObject()
                    obj = ary[j] as! NSManagedObject
                    aryTempdata.add(obj.value(forKey: "categoryList"))
                }
                let arydata = aryTempdata.object(at: 0)as! NSArray
                self.arraySection = arydata.mutableCopy()as! NSMutableArray
                self.arryForTempSend = NSMutableArray()
            }
            
           
            for item in self.arraySection {
                let dict = NSMutableDictionary()
                let aryCurruntCompetitorMappingList = NSMutableArray()
                let aryConsideringCompetitorMappingList = NSMutableArray()
                let strCID = "\(String(describing: (item as AnyObject).value(forKey: "CategoryId")!))"
                dict.setValue("0", forKey: "VisitorCategoryMapId")
                dict.setValue(strCID, forKey: "CategoryId")
                dict.setValue("", forKey: "Budget")
                dict.setValue("Select Interested Level", forKey: "InterestLevel")
                dict.setValue("", forKey: "Comment")
                dict.setValue(aryConsideringCompetitorMappingList, forKey: "ConsideringCompetitorMappingList")
                dict.setValue(aryCurruntCompetitorMappingList, forKey: "CurruntCompetitorMappingList")
                self.arryForTempSend.add(dict)
            }
            
            self.tv.dataSource = self
            self.tv.delegate = self
            self.tv.reloadData()
            

        }
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
 
        btnSave.tag = 1
        btnSaveWithGreeting.tag = 2
        self.btnSave.layer.borderWidth = 1.0
        self.btnSave.layer.borderColor = UIColor.darkGray.cgColor
        self.btnSave.layer.cornerRadius = 8.0
        self.btnSave.clipsToBounds = true
       
        self.btnSaveWithGreeting.layer.borderWidth = 1.0
        self.btnSaveWithGreeting.layer.borderColor = UIColor.darkGray.cgColor
        self.btnSaveWithGreeting.layer.cornerRadius = 8.0
        self.btnSaveWithGreeting.clipsToBounds = true
        
        
        self.btnAppoimentOK.layer.borderWidth = 1.0
        self.btnAppoimentOK.layer.borderColor = UIColor.lightGray.cgColor
        self.btnAppoimentOK.layer.cornerRadius = 8.0
       
        self.btnAppoimentClose.layer.borderWidth = 1.0
        self.btnAppoimentClose.layer.borderColor = UIColor.lightGray.cgColor
        self.btnAppoimentClose.layer.cornerRadius = 8.0
        
        self.btnAppoimentNoAppoiment.layer.borderWidth = 1.0
        self.btnAppoimentNoAppoiment.layer.borderColor = UIColor.lightGray.cgColor
        self.btnAppoimentNoAppoiment.layer.cornerRadius = 8.0

        self.viewAppoiment.layer.cornerRadius = 6.0
        self.viewAppoiment.clipsToBounds = true
        
        self.btnDone.layer.borderWidth = 1.0
        self.btnDone.layer.borderColor = UIColor.lightGray.cgColor
        self.btnDone.layer.cornerRadius = 8.0

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- ----------ALL IBACTION----------
    //MARK:-
    @IBAction func actionBtnPicker(_ sender: Any) {
        
        if picker.tag == 1 {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            txtAppoimentDate.text = formatter.string(from: picker.date)

        }else if picker.tag == 2{
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm aa"
            txtAppoimentTime.text = formatter.string(from: picker.date)
        }else{
        }
        viewDateTimePicker.isHidden = true
    }

    
    @IBAction func actionOnAppoimentCancleView(_ sender: UIButton) {
        
        if txtAppoimentDate.text?.count == 0 {
            iToast.makeText("Please select appointment date.").show()
        }else if(txtAppoimentTime.text?.count == 0){
            iToast.makeText("Please select appointment time.").show()
        }else if(txtEmploye.text?.count == 0){
            iToast.makeText("Please select employee name.").show()
        }else{
            dictVisitorData.setValue(txtAppoimentDate.text, forKey: "AppointmentDate")
            dictVisitorData.setValue(txtAppoimentTime.text, forKey: "AppointmentTime")
            dictVisitorData.setValue("\(self.dictUserData.value(forKey: "LoginId")!)", forKey: "LoginId")
            dictVisitorData.setValue("\(txtEmploye.tag)", forKey: "SubmittedBy")
            
            if btnAppoimentNoAppoiment.tag == 999 {
                dictVisitorData.setValue("SaveWithGreeting", forKey: "SaveType")
            }else{
                dictVisitorData.setValue("SaveOnly", forKey: "SaveType")
            }
            saveAllDataAndSend()
        }
    }
    
    
    @IBAction func actionOnAppoimentSaveView(_ sender: UIButton) {
        
        print(self.dictUserData)
        
        txtAppoimentDate.text = ""
        txtAppoimentTime.text = ""
        dictVisitorData.setValue("", forKey: "AppointmentDate")
        dictVisitorData.setValue("", forKey: "AppointmentTime")
        dictVisitorData.setValue("\(self.dictUserData.value(forKey: "LoginId")!)", forKey: "LoginId")
        dictVisitorData.setValue("", forKey: "SubmittedBy")

        if btnAppoimentNoAppoiment.tag == 999 {
            dictVisitorData.setValue("SaveWithGreeting", forKey: "SaveType")
        }else{
            dictVisitorData.setValue("SaveOnly", forKey: "SaveType")
        }
        viewAppoiment.isHidden = true
        btnTransprant.isHidden = true
        saveAllDataAndSend()
    }
    
    @IBAction func actionAppoimentDate(_ sender: Any) {
       
        viewDateTimePicker.isHidden = false
        picker.tag = 1
        picker.datePickerMode = UIDatePickerMode.date
        picker.isHidden = false
        tvForCompanyMemberlist.isHidden = true
        btnAppoimentClose.setTitle("OK", for: .normal)
    }
    @IBAction func actionAppoimenttime(_ sender: Any) {
        viewDateTimePicker.isHidden = false
        picker.tag = 2
        picker.datePickerMode = UIDatePickerMode.time
        picker.minuteInterval = 10
        picker.isHidden = false
        tvForCompanyMemberlist.isHidden = true
        btnAppoimentClose.setTitle("OK", for: .normal)
    }
    @IBAction func actionAppoimentEmploy(_ sender: Any) {
        let ary = getCompanyMemberList()
        viewDateTimePicker.isHidden = false
        picker.tag = 3
        picker.isHidden = true
        tvForCompanyMemberlist.isHidden = false
       // print(ary)
        tvForCompanyMemberlist.reloadData()
        tvForCompanyMemberlist.dataSource = self
        tvForCompanyMemberlist.delegate = self
        btnAppoimentClose.setTitle("CLOSE", for: .normal)

        
    }
    
    
    
    
    @IBAction func actionOnBAck(_ sender: Any) {
        navigationController?.popViewController(animated: true)

    }
    
    @IBAction func actionOnSave(_ sender: UIButton) {
        if sender.tag == 1 { //For Save
            btnTransprant.isHidden = false
            viewAppoiment.isHidden = false
            btnAppoimentNoAppoiment.tag = 99
        }else{ // For save With Greeting
            btnTransprant.isHidden = false
            viewAppoiment.isHidden = false
            btnAppoimentNoAppoiment.tag = 999


        }
    }
    @IBAction func btnDone(_ sender: UIButton) {
        btnTransprant.isHidden = true
        viewTblSelection.isHidden = true
    
        //For Current Provider
        if tagforCheck == 1 {
           // print(arryForSelectedDataForCurrentProvider)
          //  print(btnDone.tag)
            
        }
        //For Considering Provider
        else if( tagforCheck == 2){
        
        }
        //For Intrested

        else if( tagforCheck == 3){
            
        }
        tv.reloadData()
    }
    
    // MARK:- UITableView Delegate & DataSource
    // MARK:-
    
    func numberOfSections(in tableView: UITableView) -> Int{
        if tableView == tv {
            return arraySection.count

        }else if tableView == tvForCompanyMemberlist{
            return 1
        }else{
            return 1

        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tv {
            return int_expandedSections == section ?1 :0
        }
        else if tableView == tvForCompanyMemberlist{
            return aryCompantMemberlist.count
        }else{
            return arrayForProvider.count
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tvForProvider {
            let cell : CommonTBLCell = tvForProvider.dequeueReusableCell(withIdentifier: "providerCell") as! CommonTBLCell
            //---For Intrested level
            if tagforCheck == 3 {
                let str = arrayForProvider[indexPath.row]as? String
                let dict = arryForTempSend.object(at: btnDone.tag)as! NSMutableDictionary
                dict.setValue(str, forKey: "InterestLevel")
                arryForTempSend.replaceObject(at: btnDone.tag, with: dict)
               // print(arryForTempSend)
                btnTransprant.isHidden = true
                viewTblSelection.isHidden = true
                tv.reloadData()
            }
            else if(tagforCheck == 1){
                let dict = arrayForProvider.object(at: indexPath.row)as! NSDictionary
                if arryForSelectedDataForCurrentProvider.contains(dict){
                    arryForSelectedDataForCurrentProvider.remove(dict)
                    cell.provider_btnCheck.isHidden = true
                }
                else{
                    arryForSelectedDataForCurrentProvider.add(dict)
                    cell.provider_btnCheck.isHidden = false
                    
                }
                tvForProvider.reloadData()

                
            }else if(tagforCheck == 2){
                
                let dict = arrayForProvider.object(at: indexPath.row)as! NSDictionary
                if arryForSelectedDataForConsideringProvider.contains(dict){
                    arryForSelectedDataForConsideringProvider.remove(dict)
                    cell.provider_btnCheck.isHidden = true
                    
                }
                else{
                    arryForSelectedDataForConsideringProvider.add(dict)
                    cell.provider_btnCheck.isHidden = false
                    
                }
                tvForProvider.reloadData()
            }
        }else if tableView == tvForCompanyMemberlist{
              let dict = aryCompantMemberlist.object(at: indexPath.row)as! NSDictionary
              txtEmploye.text =  dict.value(forKey: "Name")as? String
              txtEmploye.tag = (dict.value(forKey: "LoginId")as? Int)!
              viewDateTimePicker.isHidden = true

        }else{
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tv {
        let cell = tv.dequeueReusableCell(withIdentifier: "Additionalinfo", for: indexPath as IndexPath) as! CommonTBLCell
         cell.additional_TxtCurrentProvider.layer.borderColor = UIColor.lightGray.cgColor
         cell.additional_TxtCurrentProvider.layer.borderWidth = 1.0
        
        cell.addtional_txtConsideringProvider.layer.borderColor = UIColor.lightGray.cgColor
        cell.addtional_txtConsideringProvider.layer.borderWidth = 1.0
       
        //For Select ConsideringProvider
    
        cell.additional_btnConsideringProvider.tag = indexPath.section
        cell.additional_btnConsideringProvider.addTarget(self, action: #selector(self.selectConsideringProvider(sender:)), for: .touchUpInside)

       //For Select Intrested
        cell.additional_btnCurrentProvider.tag = indexPath.section
        cell.additional_btnCurrentProvider.addTarget(self, action: #selector(self.selectProvider(sender:)), for: .touchUpInside)
            
        //For Select Intrested
        cell.additional_btnSelected.tag = indexPath.section
        cell.additional_btnSelected.addTarget(self, action: #selector(self.selectIntrested(sender:)), for: .touchUpInside)
        cell.additional_lblSelectIntrested.text = (arryForTempSend.object(at: indexPath.section)as! NSDictionary).value(forKey: "InterestLevel")as? String
        
            cell.additional_TxtCurrentProvider.text = ""
            cell.addtional_txtConsideringProvider.text = ""
       
        //show text For Current Provider
            var str = ""
            for item in arryForSelectedDataForCurrentProvider {
                
                let strCid =  "\((item as AnyObject).value(forKey: "CategoryId")!)"
                
                let dict = arraySection.object(at: indexPath.section)as! NSDictionary
                let strCateID = "\(String(describing: dict.value(forKey: "CategoryId")!))"
                
                if strCid == strCateID {
                    let strname =  "\((item as AnyObject).value(forKey: "CategoryCompetitorName")!)"
                    if str.characters.count > 0 {
                        str = str + ",\(strname)"
                    }else{
                        str = str + "\(strname)"
                    }
                    cell.additional_TxtCurrentProvider.text = str
                }else{

                }
                
            }
            
            
            //show text For Considering Provider
            var str1 = ""
            for item in arryForSelectedDataForConsideringProvider {
                
                let strCid =  "\((item as AnyObject).value(forKey: "CategoryId")!)"
                
                let dict = arraySection.object(at: indexPath.section)as! NSDictionary
                let strCateID = "\(String(describing: dict.value(forKey: "CategoryId")!))"
                
                if strCid == strCateID {
                    let strname =  "\((item as AnyObject).value(forKey: "CategoryCompetitorName")!)"
                    if str1.characters.count > 0 {
                        str1 = str1 + ",\(strname)"
                    }else{
                        str1 = str1 + "\(strname)"
                    }
                    cell.addtional_txtConsideringProvider.text = str1
                }else{

                }
                
            }
            
           //--For Comment
            cell.additional_txtComment.tag = indexPath.section
            cell.additional_txtBudget.tag = indexPath.section
            cell.additional_txtComment.text = (arryForTempSend.object(at: indexPath.section)as! NSDictionary).value(forKey: "Comment")as? String
            cell.additional_txtBudget.text = (arryForTempSend.object(at: indexPath.section)as! NSDictionary).value(forKey: "Budget")as? String
            
        return cell
        }
            
         //--For company member list
        else if tableView == tvForCompanyMemberlist{
            let cell = tvForCompanyMemberlist.dequeueReusableCell(withIdentifier: "loginuser", for: indexPath as IndexPath) as! CommonTBLCell
            let dict = aryCompantMemberlist.object(at: indexPath.row)as! NSDictionary
            cell.provider_lblTitle.text =  dict.value(forKey: "Name")as? String
            cell.provider_btnCheck.isHidden = true

            return cell

        }
        //For Provider Section
            
        else{
            let cell = tvForProvider.dequeueReusableCell(withIdentifier: "providerCell", for: indexPath as IndexPath) as! CommonTBLCell

            //---For Intrested level
            if tagforCheck == 3 {
                cell.provider_lblTitle.text = arrayForProvider[indexPath.row]as? String
                cell.provider_btnCheck.isHidden = true
            }
            //---For Intrested level
            else if (tagforCheck == 1){
                let dict = arrayForProvider.object(at: indexPath.row)as! NSDictionary
                  cell.provider_lblTitle.text = dict.value(forKey: "CategoryCompetitorName")as? String
                if arryForSelectedDataForCurrentProvider.contains(dict){
                    cell.provider_btnCheck.isHidden = false
                }
                else{
                    cell.provider_btnCheck.isHidden = true
                }

            
                
                
            }
           //---For Intrested level
            else if (tagforCheck == 2){
                
                let dict = arrayForProvider.object(at: indexPath.row)as! NSDictionary
                cell.provider_lblTitle.text = dict.value(forKey: "CategoryCompetitorName")as? String
                if arryForSelectedDataForConsideringProvider.contains(dict){
                    cell.provider_btnCheck.isHidden = false
                }
                else{
                    cell.provider_btnCheck.isHidden = true
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == tv {
            return 308.0;

        }else{
        return UITableViewAutomaticDimension;
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tv {

        let headerView = UIView()
        var subView = UIView()
        var subView1 = UIView()
        var subView2 = UIView()
        var headerTitle = UILabel()
        var imageView1 = UIImageView()
        
        if (int_expandedSections == section) {
            
            subView = UIView.init(frame: CGRect(x:0, y: 0, width: tv.frame.size.width, height: 40))
            subView2 = UIView.init(frame: CGRect(x:0, y: 39, width: tv.frame.size.width, height: 1))
            
            subView.backgroundColor = UIColor.white
            subView2.backgroundColor = UIColor.lightGray
            
            subView1.backgroundColor = UIColor.lightGray
            subView1 = UIView.init(frame: CGRect(x:0, y: 0, width: tv.frame.size.width, height: 1))
            
            headerTitle = UILabel.init(frame: CGRect(x:40, y: 0, width: tv.frame.size.width, height: 40))
            imageView1 = UIImageView.init(frame: CGRect(x:8, y: 0, width: 25, height: 40))
            imageView1.image = UIImage(named:"check")
            imageView1.contentMode = .center

            let dict = arraySection.object(at: section)as! NSDictionary
            headerTitle.text = "\(String(describing: dict.value(forKey: "CategoryName")!))"
            
            headerTitle.textColor = UIColor.black
            
          //  headerTitle.textAlignment = .center
            
            subView.addSubview(headerTitle)
            subView.addSubview(imageView1)
            subView.addSubview(subView2)
            subView.addSubview(subView1)

            
        } else {
            
            subView = UIView.init(frame: CGRect(x:0, y: 0, width: tv.frame.size.width, height: 80))
            
            subView.backgroundColor = UIColor.white
            subView2.backgroundColor = UIColor.lightGray
            subView2 = UIView.init(frame: CGRect(x:0, y: 39, width: tv.frame.size.width, height: 1))
            
            subView1.backgroundColor = UIColor.lightGray
            subView1 = UIView.init(frame: CGRect(x:0, y: 0, width: tv.frame.size.width, height: 1))

            headerTitle = UILabel.init(frame: CGRect(x:40, y: 0, width: tv.frame.size.width, height: 40))
            let dict = arraySection.object(at: section)as! NSDictionary
            headerTitle.text = "\(String(describing: dict.value(forKey: "CategoryName")!))"
            headerTitle.textColor = UIColor.black
            imageView1 = UIImageView.init(frame: CGRect(x:8, y: 0, width: 25, height: 40))
            imageView1.image = UIImage(named:"check_box")
            imageView1.contentMode = .center
            subView.addSubview(headerTitle)
            subView.addSubview(imageView1)
            subView.addSubview(subView2)
            subView.addSubview(subView1)

        }
        
        let headerTapped = UITapGestureRecognizer(target: self, action: #selector(self.sectionHeaderTapped))
        headerView.tag = section
        headerView.addGestureRecognizer(headerTapped)
        headerView.addSubview(subView)
        return headerView
        }else{
         return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         if tableView == tv {
            return int_expandedSections == section ?50 :50
        
         }else{
         return 0
        }
    }
    
 
    @objc private func sectionHeaderTapped(gestureRecognizer: UITapGestureRecognizer) {
        
        if(int_expandedSections == gestureRecognizer.view?.tag){
            //int_expandedSections = -1
            int_expandedSections = gestureRecognizer.view?.tag

        }
        else{
            int_expandedSections = gestureRecognizer.view?.tag
            //int_expandedSections = -1
        }
        self.tv.reloadData()
        
    }
    
    
    //MARK:
    //MARK: -----------ALL API CALLING---------
    func CallCategoryAPI() {
            if checkInternetConnection() == true {
             KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictUserData.value(forKey: "CompanyId")!))"
            dict.setValue(strCompanyID, forKey: "CompanyId")
            
            WebService.callAPIBYGET(parameter: dict, url: API_CategoryList, OnResultBlock: {  (result, status) in
               // print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "CategoryList")as! NSArray
                        
                        
                        deleteAllRecords(strEntity: "CategoryList")
                        saveDataInLocal(strEntity: "CategoryList", strKey: "categoryList", data: aryTemp)
                        let ary = getDataFromLocal(strEntity: "CategoryList", strkey: "categoryList")
                        let aryTempdata = NSMutableArray()
                        if ary.count > 0 {
                            for j in 0 ..< ary.count {
                                var obj = NSManagedObject()
                                obj = ary[j] as! NSManagedObject
                                aryTempdata.add(obj.value(forKey: "categoryList"))
                            }
                        
                        }
                        
                        let arydata = aryTempdata.object(at: 0)as! NSArray
                        self.arraySection = arydata.mutableCopy()as! NSMutableArray
                        self.arryForTempSend = NSMutableArray()
                        for item in self.arraySection {
                             let dict = NSMutableDictionary()
                            let aryCurruntCompetitorMappingList = NSMutableArray()
                            let aryConsideringCompetitorMappingList = NSMutableArray()
                            let strCID = "\(String(describing: (item as AnyObject).value(forKey: "CategoryId")!))"
                            dict.setValue("0", forKey: "VisitorCategoryMapId")
                            dict.setValue(strCID, forKey: "CategoryId")
                            dict.setValue("", forKey: "Budget")
                            dict.setValue("Select Interested Level", forKey: "InterestLevel")
                            dict.setValue("", forKey: "Comment")
                            dict.setValue(aryConsideringCompetitorMappingList, forKey: "ConsideringCompetitorMappingList")
                            dict.setValue(aryCurruntCompetitorMappingList, forKey: "CurruntCompetitorMappingList")
                            self.arryForTempSend.add(dict)
                        }
                        
                        self.tv.dataSource = self
                        self.tv.delegate = self
                        self.tv.reloadData()
                    }else{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                    }
                }
                else{
                    
                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    func callUploadAudio(audioData : Data , strname : String)  {
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            WebService.callAPIWithAudio(parameter:  NSDictionary(), url: BaseURL_AudioUpload, audioData: audioData, filename: strname, OnResultBlock: {  (result, status) in
               // print(result);
                //KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let strstatus = result.value(forKey: "message")as! String
                    if strstatus == "SUCCESS"{
                        
                    }else{
                        let strstatus = result.value(forKey: "message")as! String
                        iToast.makeText(strstatus).show()
                    }
                }
                else{
                    
                    
                }
                
            })
            

        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    
    func callUploadImage(imageData : UIImage , strname : String)  {
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            
            WebService.callAPIWithImageOnlyOne(parameter: NSDictionary(), url: BaseURL_ImageUpload, image: imageData, imageName: strname, OnResultBlock: {  (result, status) in
               // print(result);
                if status == "Suceess"{
                    let strstatus = result.value(forKey: "message")as! String
                    if strstatus == "SUCCESS"{
                        self.apiCallForTag = self.apiCallForTag + 1
                        if self.apiCallForTag == self.apiCallingTag {
                            //self.CallAdd_NEWLead()
                        }
                    }else{
                        KRProgressHUD.dismiss()
                      let strstatus = result.value(forKey: "message")as! String
                        iToast.makeText(strstatus).show()
                    }
                }
                else{
                    KRProgressHUD.dismiss()

                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    
    
    func CallAdd_NEWLead() {
        if checkInternetConnection() == true {
            print(dictVisitorData)
            
            KRProgressHUD.show(message: "Loading...")
             WebService.callAPIBYPOST(parameter: dictVisitorData, url: API_ADDCompanyVisitor, OnResultBlock: {  (result, status) in
               // print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                        for aViewController in viewControllers {
                            if aViewController is EventVC {
                                self.navigationController!.popToViewController(aViewController, animated: true)
                            }
                        }
                    }else{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                       
                    }
                }
                else{
                    
                    
                }
                
            })

        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    
    //MARK:
    //MARK: -----------TEXT FILED DELEGATE METHOD----------
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //
        textField.resignFirstResponder()
        
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        if textField.placeholder == "Budget" {
            // for Comment
            let dict = arryForTempSend.object(at: textField.tag)as! NSMutableDictionary
            dict.setValue(textField.text, forKey: "Budget")
            arryForTempSend.replaceObject(at: textField.tag, with: dict)
           // print(arryForTempSend)
            tv.reloadData()
        }else{
            // for Comment
            let dict = arryForTempSend.object(at: textField.tag)as! NSMutableDictionary
            dict.setValue(textField.text, forKey: "Comment")
            arryForTempSend.replaceObject(at: textField.tag, with: dict)
           // print(arryForTempSend)
            tv.reloadData()
        }
        
      
       
        
    }

    //MARK:
    //MARK: ----------PRIVATE FUNCTION---------
    
    func saveImageOnDocument(image : UIImage , strname : String)  {
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        // create a name for your image
        let fileURL = documentsDirectoryURL.appendingPathComponent("\(strname)")
        if !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try  UIImageJPEGRepresentation(image,0.0)!.write(to: fileURL)
                print("Image Added Successfully")
            } catch {
                print(error)
            }
        } else {
            print("Image Not Added")
        }
    }
    
  
    func getCompanyMemberList() -> NSMutableArray {
        let aryTemp = getDataFromLocal(strEntity: "CompanyMember", strkey: "member")
        let aryTempOutboxdata = NSMutableArray()
        if aryTemp.count > 0 {
            for j in 0 ..< aryTemp.count {
                var obj = NSManagedObject()
                obj = aryTemp[j] as! NSManagedObject
                aryTempOutboxdata.add(obj.value(forKey: "member"))
            }
           // print(aryTempOutboxdata)
            aryCompantMemberlist = NSMutableArray()
            aryCompantMemberlist = (aryTempOutboxdata.object(at: 0)as! NSArray).mutableCopy()as! NSMutableArray
            return aryCompantMemberlist
        }
        return NSMutableArray()
    }
    
    
    
    
    func saveAllDataAndSend()  {

        //---For Current
        for j in 0 ..< arryForTempSend.count {
            let ary = NSMutableArray()
            let dict  = arryForTempSend.object(at: j)as! NSDictionary
            let  strCategID1 = "\(dict.value(forKey: "CategoryId")!)"
            
            for i in 0 ..< arryForSelectedDataForCurrentProvider.count {
                let dict  = arryForSelectedDataForCurrentProvider.object(at: i)as! NSDictionary
                let  strCategID = "\(dict.value(forKey: "CategoryId")!)"
                let  strCategoryCompetitorId = "\(dict.value(forKey: "CategoryCompetitorId")!)"
                if strCategID == strCategID1 {
                    let dict = NSMutableDictionary()
                    dict.setValue(strCategoryCompetitorId, forKey: "CurruntCategoryCompetitorId")
                    ary.add(dict)
                    let dictTemp = (arryForTempSend.object(at: j)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    dictTemp.setValue(ary, forKey: "CurruntCompetitorMappingList")
                    arryForTempSend.replaceObject(at: j, with: dictTemp)
                }else{
                    
                }
            }
        }
       
        //--For ConsideringProvider
        for j in 0 ..< arryForTempSend.count {
            let ary = NSMutableArray()
            let dict  = arryForTempSend.object(at: j)as! NSDictionary
            let  strCategID1 = "\(dict.value(forKey: "CategoryId")!)"
            for i in 0 ..< arryForSelectedDataForConsideringProvider.count {
                let dict  = arryForSelectedDataForConsideringProvider.object(at: i)as! NSDictionary
                let  strCategID = "\(dict.value(forKey: "CategoryId")!)"
                let  strCategoryCompetitorId = "\(dict.value(forKey: "CategoryCompetitorId")!)"
                if strCategID == strCategID1 {
                    let dict = NSMutableDictionary()
                    dict.setValue(strCategoryCompetitorId, forKey: "ConsideringCategoryCompetitorId")
                    ary.add(dict)
                    let dictTemp = (arryForTempSend.object(at: j)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    dictTemp.setValue(ary, forKey: "ConsideringCompetitorMappingList")
                    arryForTempSend.replaceObject(at: j, with: dictTemp)
                }else{
                    
                }
            }
        }
        
        let strCompanyID = "\(String(describing: dictUserData.value(forKey: "CompanyId")!))"
        dictVisitorData.setValue(strCompanyID, forKey: "CompanyId")
        dictVisitorData.setValue(arryForTempSend, forKey: "CategoryList")
        print(dictVisitorData)
        if checkInternetConnection() == true {
            if image1 != UIImage(named:"defult_img") {
                apiCallingTag =  apiCallingTag + 1
            }
            if image2 != UIImage(named:"defult_img") {
                apiCallingTag = apiCallingTag + 1
            }
            if image3 != UIImage(named:"defult_img") {
                apiCallingTag = apiCallingTag + 1
            }
            if image4 != UIImage(named:"defult_img") {
                apiCallingTag = apiCallingTag + 1
            }
            if image1 == UIImage(named:"defult_img") {
                
            }else{
                callUploadImage(imageData: image1, strname: strImageName1)
            }
            if image2 == UIImage(named:"defult_img") {
            }else{
                callUploadImage(imageData: image2, strname: strImageName2)
            }
            
            if image3 == UIImage(named:"defult_img") {
            }else{
                callUploadImage(imageData: image3, strname: strImageName3)
            }
            if image4 == UIImage(named:"defult_img") {
            }else{
                callUploadImage(imageData: image4, strname: strImageName4)
            }
            if (audio_Data.count > 0 ) {
                callUploadAudio(audioData: audio_Data, strname: strVoiceName)
            }
            if apiCallForTag == apiCallingTag {
                //CallAdd_NEWLead()
            }
            CallAdd_NEWLead()
        }else{
            DispatchQueue.main.async {
                self.saveImageOnDocument(image : image1 , strname : strImageName1)
                self.saveImageOnDocument(image : image2 , strname : strImageName2)
                self.saveImageOnDocument(image : image3 , strname : strImageName3)
                self.saveImageOnDocument(image : image4 , strname : strImageName4)
              //  dictVisitorData.setValue(image1, forKey: "img1")
                //dictVisitorData.setValue(image2, forKey: "img2")
               // dictVisitorData.setValue(image3, forKey: "img3")
               // dictVisitorData.setValue(image4, forKey: "img4")
                dictVisitorData.setValue(audio_Data, forKey: "audionamedata")
                dictVisitorData.setValue(strImageName1, forKey: "img1Name")
                dictVisitorData.setValue(strImageName2, forKey: "img2Name")
                dictVisitorData.setValue(strImageName3, forKey: "img3Name")
                dictVisitorData.setValue(strImageName4, forKey: "img4Name")
                dictVisitorData.setValue(strVoiceName, forKey: "audioname")
                saveDataInLocalDict(strEntity: "OutBox", strKey: "outbox", data: dictVisitorData)
                iToast.makeText("Save in Outbox").show()
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                for aViewController in viewControllers {
                    if aViewController is EventVC {
                        self.navigationController!.popToViewController(aViewController, animated: true)
                    }
                }
            }
        }
    }
    func covertArrayToJsonString(object: NSMutableArray) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }

    @objc func selectProvider(sender : UIButton) {
        btnTransprant.isHidden = false
        viewTblSelection.isHidden = false
        lblTblTitle.text = "Select Current Provider"
        arrayForProvider = NSMutableArray()
        arrayForProvider = ((arraySection.object(at: sender.tag)as! NSDictionary).value(forKey: "CategoryCompetitorList")as! NSArray).mutableCopy()as! NSMutableArray
        tagforCheck = 1
        btnDone.tag = sender.tag
        tvForProvider.delegate = self
        tvForProvider.dataSource = self
        tvForProvider.reloadData()
    }
    @objc func selectConsideringProvider(sender : UIButton) {
        btnTransprant.isHidden = false
        viewTblSelection.isHidden = false
        arrayForProvider = NSMutableArray()
        arrayForProvider = ((arraySection.object(at: sender.tag)as! NSDictionary).value(forKey: "CategoryCompetitorList")as! NSArray).mutableCopy()as! NSMutableArray
        tagforCheck = 2
        btnDone.tag = sender.tag
        tvForProvider.delegate = self
        tvForProvider.dataSource = self
        tvForProvider.reloadData()
        lblTblTitle.text = "Select Considering Provider"
    }
    @objc func hidePopUP(sender : UIButton) {
        btnTransprant.isHidden = true
        viewTblSelection.isHidden = true
        viewAppoiment.isHidden = true
    }
    @objc func selectIntrested(sender: UIButton){
        
        btnTransprant.isHidden = false
        viewTblSelection.isHidden = false
        lblTblTitle.text = "Select Intrested level"
          tagforCheck = 3
          btnDone.tag = sender.tag
          arrayForProvider = NSMutableArray()
          arrayForProvider = aryIntrested
          tvForProvider.delegate = self
          tvForProvider.dataSource = self
          tvForProvider.reloadData()
    }

}
