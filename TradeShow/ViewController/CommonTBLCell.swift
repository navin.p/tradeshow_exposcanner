//
//  CommonTBLCell.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/3/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit

class CommonTBLCell: UITableViewCell {

    
    //--Home Cell
    
    @IBOutlet weak var home_lblTitle: UILabel!
    
    @IBOutlet weak var home_lblSubTitle: UILabel!
    
    @IBOutlet weak var home_lblLocation: UILabel!
    
    @IBOutlet weak var home_lblDate: UILabel!
    
    @IBOutlet weak var home_lblTime: UILabel!
    @IBOutlet weak var home_imgEvent: UIImageView!

    //EventSearch
    @IBOutlet weak var eventS_img: UIImageView!
    
    @IBOutlet weak var eventS_lblName: UILabel!
    
    @IBOutlet weak var eventS_lblRole: UILabel!
    
    @IBOutlet weak var eventS_lblContactNumber: UILabel!
    
    @IBOutlet weak var eventS_Email: UILabel!
    
    //Detail Cell
    
    @IBOutlet weak var detail_lblVisitorid: UILabel!
    @IBOutlet weak var detail_lblContactNumber: UILabel!
    
    @IBOutlet weak var detail_lblEmail: UILabel!
    
    @IBOutlet weak var detail_lblPossition: UILabel!
    
    @IBOutlet weak var detail_lblCompanyName: UILabel!
    
    @IBOutlet weak var detail_lblCompanySize: UILabel!
    
    @IBOutlet weak var detail_lblAddressServices: UILabel!
    
    @IBOutlet weak var detail_lblComment: UILabel!
    
    @IBOutlet weak var detail_lblNofile: UILabel!
   

    
    
    @IBOutlet weak var detail_Img1: UIImageView!
    
    @IBOutlet weak var detail_img2: UIImageView!
    
    @IBOutlet weak var detail_img3: UIImageView!
    
    @IBOutlet weak var detail_img4: UIImageView!
    
    
    @IBOutlet weak var DETAIL_VOICEVIEW: UIView!
    @IBOutlet weak var detail_Play: UIButton!

    
    //For Company Size list
    
    @IBOutlet weak var company_lblList: UILabel!
    
    @IBOutlet weak var company_checkicon: UIButton!
    
    //FOr Additional Info
    
    @IBOutlet weak var additional_TxtCurrentProvider: UITextView!
    
    @IBOutlet weak var addtional_txtConsideringProvider: UITextView!
    
    @IBOutlet weak var additional_txtBudget: SkyFloatingLabelTextField!
      @IBOutlet weak var additional_txtComment: SkyFloatingLabelTextField!
    
    @IBOutlet weak var additional_lblSelectIntrested: UILabel!
        @IBOutlet weak var additional_btnSelected: UIButton!
    
    @IBOutlet weak var additional_btnConsideringProvider: UIButton!
    
    @IBOutlet weak var additional_btnCurrentProvider: UIButton!
    
    @IBOutlet weak var provider_lblTitle: UILabel!
    @IBOutlet weak var provider_btnCheck: UIButton!

    
    //For provider
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        // Configure the view for the selected state
    }

}
