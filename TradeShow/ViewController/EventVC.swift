//
//  EventDetailVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/4/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

class EventVC: UIViewController {

    
    var dictData = NSMutableDictionary()
    var aryImportdblist = NSMutableArray()

    //MARK:- ---------ALL IBOUTLET ----------
    //MARK:-
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    //MARK:- ----------LIFE CYCLE ----------
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(dictData)
        lbl_Title.text = dictData.value(forKey: "Event")as? String
        if checkInternetConnection() == true{
          CallCompanyMemberAPI()
          CallCompanySizeList()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.aryImportdblist.count == 0 {
            CallEVentVisitorAPI()
        }
    }
    //MARK:
    //MARK: -----------ALL API CALLING---------
    func CallEVentVisitorAPI() {
        
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strEventId = "\(String(describing: dictData.value(forKey: "EventId")!))"
            dict.setValue(strEventId, forKey: "EventId")
            dict.setValue("", forKey: "VisitorId")

            WebService.callAPIBYGET(parameter: dict, url: API_EventVisitorList, OnResultBlock: {  (result, status) in
              print("EventVisitorList-------------------\(result)");
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    
                  
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "VisitorImportDBList")as! NSArray
                        self.aryImportdblist = aryTemp.mutableCopy() as! NSMutableArray
                        let dictdata = NSMutableDictionary()
                        dictdata.setValue(aryTemp, forKey: "VisitorImportDBList")
                        dictdata.setValue(strEventId, forKey: "EventID")

                        var strTag = 0
                        //--------ForcheckEvent are exist or not
                        let aryEventHistory = getDataFromLocal(strEntity: "EventVisitorList", strkey: "eventvisitorlist")
                        deleteAllRecords(strEntity: "EventVisitorList")
                        
                        if aryEventHistory.count > 0 {
                            for j in 0 ..< aryEventHistory.count {
                                var obj = NSManagedObject()
                                let aryTempEventHistory = NSMutableArray()
                                obj = aryEventHistory[j] as! NSManagedObject
                                aryTempEventHistory.add(obj.value(forKey: "eventvisitorlist"))
                                let strid = "\(String(describing: (aryTempEventHistory[0] as! NSDictionary).value(forKey: "EventID")!))"
                                if strid == strEventId  {
                                    saveDataInLocalDict(strEntity: "EventVisitorList", strKey: "eventvisitorlist", data: dictdata)
                                    strTag = 1
                                }else{
                                    saveDataInLocalDict(strEntity: "EventVisitorList", strKey: "eventvisitorlist", data: obj.value(forKey: "eventvisitorlist") as! NSMutableDictionary)
                                }
                            }
                            if strTag == 0{
                                saveDataInLocalDict(strEntity: "EventVisitorList", strKey: "eventvisitorlist", data: dictdata)
                                
                            }
                        }
                            //---------FirstTime---------
                            
                        else{
                            saveDataInLocalDict(strEntity: "EventVisitorList", strKey: "eventvisitorlist", data: dictdata)
                        }
                        
                    }else{
                        //let message =  dict.value(forKey: "Success")as! String
                        //iToast.makeText(message).show()
                    }
                }
                else{
                    
                    
                }
                
            })
        }
        else{
           // iToast.makeText("Please check internet connection.").show()
        }
    }
    func CallCompanyMemberAPI() {
        
        if checkInternetConnection() == true {
            // KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictData.value(forKey: "CompanyId")!))"
            dict.setValue(strCompanyID, forKey: "CompanyId")
            
            WebService.callAPIBYGET(parameter: dict, url: API_COMPANTMEMBER_LIST, OnResultBlock: {  (result, status) in
                print(result);
               // KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    
                    
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "CompanyLoginList")as! NSArray
                        deleteAllRecords(strEntity:"CompanyMember")
                        saveDataInLocal(strEntity: "CompanyMember", strKey: "member", data: aryTemp as NSArray)
                        
                    }else{
                        //let message =  dict.value(forKey: "Success")as! String
                        //iToast.makeText(message).show()
                    }
                }
                else{
                    
                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    func CallCompanySizeList() {
        
        if checkInternetConnection() == true {
           // KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictData.value(forKey: "CompanyId")!))"
            dict.setValue(strCompanyID, forKey: "CompanyId")
            WebService.callAPIBYGET(parameter: dict, url: API_COMPANYSIZE, OnResultBlock: {  (result, status) in
                print(result);
               // KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "CompanySizeList")as! NSArray
                        deleteAllRecords(strEntity:"ComanySizeList")
                        saveDataInLocal(strEntity: "ComanySizeList", strKey: "size", data: aryTemp as NSArray)
                    }else{
                       // let message =  dict.value(forKey: "Success")as! String
                       // iToast.makeText(message).show()
                    }
                }
                else{
                    
                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    //MARK:- ----------All IBACTION ----------
    //MARK:-
    @IBAction func actionOnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func actionOnDetail(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventSearchVC") as! EventSearchVC
        vc.dictData = self.dictData
        vc.strCome = "outbox"
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    @IBAction func actionOnNewLead(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewLeadVC") as! AddNewLeadVC
        vc.dictData = self.dictData
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func actionOnHistory(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventSearchVC") as! EventSearchVC
         vc.dictData = self.dictData
        vc.strCome = "history"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}
