//
//  LogInVC.swift
//  TradeShow
//
//  Created by Navin Patidar on 10/3/17.
//  Copyright © 2017 Saavan_patidar. All rights reserved.
//

import UIKit
import CoreData

class LogInVC: UIViewController , UITextFieldDelegate {

    var isSubmitButtonPressed = false
    var showingTitleInProgress = false
    var dictUserData = NSDictionary()

    @IBOutlet weak var txtUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnKeepMe: UIButton!
    
    //MARK:
    //MARK: -----------Life Cycle----------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnLogin.layer.borderWidth = 1.0
         self.btnLogin.layer.borderColor = UIColor.lightGray.cgColor
         self.btnLogin.layer.cornerRadius = 8.0
       // txtPassword.text = "navin"
       // txtUserName.text = "navin.p@infocratsweb.com"
        
        //txtPassword.text = "priyansi"
       // txtUserName.text = "priyansi@infocratsweb.com"
        txtPassword.delegate = self
        txtUserName.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:
    //MARK: -----------ALL IBACTION----------
    
    @IBAction func actionOnKeepMe(_ sender: UIButton) {
        
        if btnKeepMe.currentImage == UIImage(named:"check_box") {
            btnKeepMe.setImage(UIImage(named:"check"), for: .normal)
        }else{
            btnKeepMe.setImage(UIImage(named:"check_box"), for: .normal)
        }
    }
    
    
    @IBAction func actionOnlogin(_ sender: Any) {
        
        self.isSubmitButtonPressed = true
        
        if !self.txtUserName.hasText {
            self.showingTitleInProgress = true
            self.txtUserName.setTitleVisible(true, animated: true, animationCompletion: self.showingTitleInAnimationComplete)
            self.txtUserName.isHighlighted = true
            iToast.makeText("Please Enter User Name.").show()

        }
        else if !self.txtPassword.hasText {
            self.showingTitleInProgress = true
            self.txtPassword.setTitleVisible(true, animated: true, animationCompletion: self.showingTitleInAnimationComplete)
            self.txtPassword.isHighlighted = true
            iToast.makeText("Please Enter Password.").show()

        }
        
        else{
        
           CallLogInAPI()
        
        }
        self.isSubmitButtonPressed = false
        if(!self.showingTitleInProgress) {
            self.hideTitleVisibleFromFields()
        }
        
    }
    //MARK:
    //MARK: -----------TEXT FILED DELEGATE METHOD----------
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //
        textField.resignFirstResponder()
        
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtUserName)
        {
            if ((txtUserName.text?.characters.count)! > 0) {
                
                if ((txtUserName.text?.characters.count)! > 30) {
                    if (string == "")
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            if (string == " ") {
                return false;
            }
        }
        if(textField == txtPassword)
        {
            if ((txtPassword.text?.characters.count)! > 0) {
                
                if ((txtPassword.text?.characters.count)! > 12) {
                    if (string == "")
                    {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            if (string == " ") {
                return false;
            }
        }

        return true
    }
    //MARK:
    //MARK: -----------ALL PRIVATE FUNCTION----------
    
    func showingTitleInAnimationComplete() {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.20) {
            self.showingTitleInProgress = false
            if(!self.isSubmitButtonPressed) {
                self.hideTitleVisibleFromFields()
            }
        }
    }
    
    func hideTitleVisibleFromFields() {
        
        self.txtUserName.setTitleVisible(false, animated: true)
        self.txtUserName.isHighlighted = false
        self.txtPassword.setTitleVisible(false, animated: true)
        self.txtPassword.isHighlighted = false
        
    }
    
    
    func isValidEmail(str:String?) -> Bool {
        let emailRegEx = "[A-Za-z0-9._]+@[A-Za-z0-9]+\\.[A-Za-z]{2,4}";
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: str)
        return result
    }
    
    //MARK:
    //MARK: -----------ALL API CALLING---------
       func CallCompanyMemberAPI() {
        
        if checkInternetConnection() == true {
            // KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictUserData.value(forKey: "CompanyId")!))"
            dict.setValue(strCompanyID, forKey: "CompanyId")
            
            WebService.callAPIBYGET(parameter: dict, url: API_COMPANTMEMBER_LIST, OnResultBlock: {  (result, status) in
              //  print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    
                    
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "CompanyLoginList")as! NSArray
                        deleteAllRecords(strEntity:"CompanyMember")
                        saveDataInLocal(strEntity: "CompanyMember", strKey: "member", data: aryTemp as NSArray)
                        
                    }else{
                        //let message =  dict.value(forKey: "Success")as! String
                        //iToast.makeText(message).show()
                    }
                }
                else{
                    
                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    func CallCompanySizeList() {
        
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictUserData.value(forKey: "CompanyId")!))"
            dict.setValue(strCompanyID, forKey: "CompanyId")
            WebService.callAPIBYGET(parameter: dict, url: API_COMPANYSIZE, OnResultBlock: {  (result, status) in
               // print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "CompanySizeList")as! NSArray
                        deleteAllRecords(strEntity:"ComanySizeList")
                        saveDataInLocal(strEntity: "ComanySizeList", strKey: "size", data: aryTemp as NSArray)
                    }else{
                        // let message =  dict.value(forKey: "Success")as! String
                        // iToast.makeText(message).show()
                    }
                }
                else{
                    
                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    func CallCategoryAPI() {
        if checkInternetConnection() == true {
            //KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            let strCompanyID = "\(String(describing: dictUserData.value(forKey: "CompanyId")!))"
            dict.setValue(strCompanyID, forKey: "CompanyId")
            
            WebService.callAPIBYGET(parameter: dict, url: API_CategoryList, OnResultBlock: {  (result, status) in
               // print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                    let dict = result.value(forKey: "data")as! NSDictionary
                    let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        let aryTemp = dict.value(forKey: "CategoryList")as! NSArray
                        deleteAllRecords(strEntity: "CategoryList")
                        saveDataInLocal(strEntity: "CategoryList", strKey: "categoryList", data: aryTemp)
                       }else{
                       // let message =  dict.value(forKey: "Success")as! String
                       // iToast.makeText(message).show()
                    }
                }
                else{
                    
                    
                }
                
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    
    
   //---------------------------------------------------------//
    
    
    
    func CallLogInAPI() {
       
        if checkInternetConnection() == true {
            KRProgressHUD.show(message: "Loading...")
            let dict = NSMutableDictionary()
            dict.setValue(txtUserName.text, forKey: "UserName")
            dict.setValue(txtPassword.text, forKey: "Password")
            WebService.callAPIBYGET(parameter: dict, url: API_LOGIN, OnResultBlock: {  (result, status) in
               // print(result);
                KRProgressHUD.dismiss()
                if status == "Suceess"{
                let dict = result.value(forKey: "data")as! NSDictionary
                let strstatus = dict.value(forKey: "Result")as! String
                    if strstatus == "True"{
                        DispatchQueue.main.async {
                            
                            if self.btnKeepMe.currentImage == UIImage(named:"check_box") {
                                nsud.set("NO", forKey: "LoginStatus")
                                nsud.synchronize()
                            }else{
                                nsud.set("YES", forKey: "LoginStatus")
                                nsud.synchronize()
                            }
                            
                           
                            let dictTemp = dict.value(forKey: "LoginList")as! NSDictionary
                            deleteAllRecords(strEntity:"LogInData")
                            saveDataInLocalDict(strEntity: "LogInData", strKey: "logindata", data: dictTemp.mutableCopy()as! NSMutableDictionary)
                            
                            
                            let aryTemp = getDataFromLocal(strEntity: "LogInData", strkey: "logindata")
                            
                            let aryTempdata = NSMutableArray()
                            if aryTemp.count > 0 {
                                
                                
                                for j in 0 ..< aryTemp.count {
                                    var obj = NSManagedObject()
                                    obj = aryTemp[j] as! NSManagedObject
                                    aryTempdata.add(obj.value(forKey: "logindata"))
                                }
                              //  print(aryTempdata)
                                self.dictUserData = NSDictionary()
                                self.dictUserData = aryTempdata.object(at: 0)as! NSDictionary
                            }
                            
                            self.CallCompanyMemberAPI()
                            self.CallCompanySizeList()
                            self.CallCategoryAPI()
                            let mainVcIntial = kConstantObj.SetIntialMainViewController(aStoryBoardID: "HomeVC")
                            appDelegate.window?.rootViewController = mainVcIntial
                            
                            
                        }
                    }else{
                        let message =  dict.value(forKey: "Success")as! String
                        iToast.makeText(message).show()
                    }
                }
                else{
                
                
                }
           
            })
        }
        else{
            iToast.makeText("Please check internet connection.").show()
        }
    }
    


}
