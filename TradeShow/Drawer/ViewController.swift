//
//  ViewController.swift
//  WireFraming
//
//  Created by admin on 26/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    var dicoLocalisation = NSDictionary()
    var strTag : Int?
    
    @IBOutlet weak var tv: UITableView!
    private let aryMenuItems = ["Home","History","OutBox","Logout"] as NSArray
    private let aryMenuItemsImages = ["home","drictory","box2","logout"] as NSArray

    override func viewDidLoad() {
        super.viewDidLoad()
        tv.delegate = self
        tv.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- UITableView Delegate Methods
    //MARK:-
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return aryMenuItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section != 0 {
            let cell = tv.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! DrawerCellTableViewCell
            cell.lbltitle.text = aryMenuItems[indexPath.row]as? String
            cell.imgMenu.image = UIImage(named:(aryMenuItemsImages[indexPath.row]as? String)!)
            return cell
        }else{
            let cell = tv.dequeueReusableCell(withIdentifier: "MenuCellStatic", for: indexPath as IndexPath) as! DrawerCellTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 0
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0 {
            let mainVcIntial = kConstantObj.SetMainViewController (aStoryBoardID: "HomeVC")
            appDelegate.window?.rootViewController = mainVcIntial
        }else if(indexPath.row == 1){
            let mainVcIntial = kConstantObj.SetMainViewController (aStoryBoardID: "HistoryVC")
            appDelegate.window?.rootViewController = mainVcIntial

            
        }else if(indexPath.row == 2){
            let mainVcIntial = kConstantObj.SetMainViewController (aStoryBoardID: "EventSearchVC")
            appDelegate.window?.rootViewController = mainVcIntial
            
        }else{
            let alert = UIAlertController(title: "Expo Scanner", message: "Are You Sure Want To Logout ?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.destructive, handler: { action in
                nsud.set("NO", forKey: "LoginStatus")
                nsud.synchronize()
                
                deleteAllRecords(strEntity: "CategoryList")
                deleteAllRecords(strEntity: "EventHistory")
                deleteAllRecords(strEntity: "EventList")
                deleteAllRecords(strEntity: "EventNameHistory")
                deleteAllRecords(strEntity: "EventVisitorData")
                deleteAllRecords(strEntity: "EventVisitorList")
                deleteAllRecords(strEntity: "LogInData")
                deleteAllRecords(strEntity: "OutBox")
                deleteAllRecords(strEntity: "CompanyMember")
                deleteAllRecords(strEntity: "ComanySizeList")
                
                
                let mainVcIntial = kConstantObj.SetMainViewController(aStoryBoardID: "LogInVC")
                appDelegate.window?.rootViewController = mainVcIntial
            }))
            alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.destructive, handler: { action in
                
            }))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
   
    }


   }






