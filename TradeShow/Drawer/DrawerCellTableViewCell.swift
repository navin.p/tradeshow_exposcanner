//
//  DrawerCellTableViewCell.swift
//  WireFraming
//
//  Created by admin on 26/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class DrawerCellTableViewCell: UITableViewCell {
  

    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none

        // Configure the view for the selected state
    }

}
