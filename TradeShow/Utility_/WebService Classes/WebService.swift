//
//  WebService.swift
//  AlamoFire_WebService
//
//  Created by admin on 22/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class WebService: NSObject,NSURLConnectionDelegate,XMLParserDelegate {
    
    
    class func callSOAP_APIBY_POST(soapMessage:String,url:String,type:String, OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
   
        let r = Reachability(hostname: "www.google.com")
        
        if r?.currentReachabilityStatus != Reachability.NetworkStatus.reachableViaWiFi && r?.currentReachabilityStatus==Reachability.NetworkStatus.reachableViaWWAN {
            
            let dic = NSMutableDictionary.init()
            dic.setValue("No Internet Connection, try later!", forKey: "message")
            dic.setValue("false", forKey: "status")
            OnResultBlock(dic,"failure")
        }
        else
        {
            let url1 = URL(string: url)
            var theRequest = URLRequest(url: url1!)
            let msgLength = soapMessage.characters.count
            theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
            theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
            theRequest.httpMethod = "POST"
            theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
            request(theRequest)
                .responseString { response in
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value
                        {
                            if let xmlString = response.result.value {
                                let xml = SWXMLHash.parse(xmlString)
                                let body =  xml["soap:Envelope"]["soap:Body"]
                                if let helpDeskElement = body["GetAllFeature_IsFavouriteByFeatureTypeResponse"]["GetAllFeature_IsFavouriteByFeatureTypeResult"].element {
                                    let getResult = helpDeskElement.text
                                    var dict = NSDictionary()
                                    dict = self.convertToDictionary(text: getResult)! as NSDictionary
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(dict, forKey: "data")
                                    OnResultBlock((dictdata) ,"Suceess")
                                    
                                }

                            
                          
                        }
                }
                        break
                        
                    case .failure(_):
                        print(response.result.error ?? 0)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("Connection Time Out ", forKey: "message")
                        OnResultBlock(dic,"failure")
                        break
                  }
        }

    
        }
    
    }
    
    
    class func callSOAP_APIBY_GETHELP(soapMessage:String,url:String,resultype:String , responcetype : String, OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        let r = Reachability(hostname: "www.google.com")
        
        if r?.currentReachabilityStatus != Reachability.NetworkStatus.reachableViaWiFi && r?.currentReachabilityStatus==Reachability.NetworkStatus.reachableViaWWAN {
            
            let dic = NSMutableDictionary.init()
            dic.setValue("No Internet Connection, try later!", forKey: "message")
            dic.setValue("false", forKey: "status")
            
            OnResultBlock(dic,"failure")
        }
        else
        {
            let url1 = URL(string: url)
            var theRequest = URLRequest(url: url1!)
            let msgLength = soapMessage.characters.count
            theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
            theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
            theRequest.httpMethod = "POST"
            theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
            request(theRequest)
                .responseString { response in
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value
                        {
                            if let xmlString = response.result.value {
                                let xml = SWXMLHash.parse(xmlString)
                                let body =  xml["soap:Envelope"]["soap:Body"]
                                if let helpDeskElement = body["\(responcetype)"]["\(resultype)"].element {
                                    let getResult = helpDeskElement.text
                                    var dict = NSDictionary()
                                    dict = self.convertToDictionary(text: getResult)! as NSDictionary
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(dict, forKey: "data")
                                    OnResultBlock((dictdata) ,"Suceess")
                                    
                                }
                                
                                
                                
                            }
                        }
                        break
                        
                    case .failure(_):
                        print(response.result.error)
                        let dic = NSMutableDictionary.init()
                        dic .setValue("Connection Time Out ", forKey: "message")
                        OnResultBlock(dic,"failure")
                        break
                    }
            }
            
            
        }
        
    }

    // MARK: - Convert String To Dictionary Function
    
   class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func convertDataToDictionary(text: NSData) -> [String: Any]? {
        if text != nil {
            do {
                return try JSONSerialization.jsonObject(with: text as Data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    
    
    //MARK:
    //MARK: API Without Secureity Parameter
    class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        if checkInternetConnection() == true {
            request(url, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                    
                case .success(_):
                    if let data = response.result.value
                    {
                        // print(data)
                        let dictdata = NSMutableDictionary()
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"Suceess")
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.result.error)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                    
                }
                
            }
            
            
        }else{
            let dic = NSMutableDictionary.init()
            dic.setValue("No Internet Connection, try later!", forKey: "message")
            dic.setValue("false", forKey: "status")
            
            OnResultBlock(dic,"failure")
        
        }
    }
    class func callAPIBYPOST(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        if checkInternetConnection() == true {
            print(parameter)
            
            
            request(url, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                    
                case .success(_):
                    if let data = response.result.value
                    {
                        //print(data)
                        let dictdata = NSMutableDictionary()
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"Suceess")
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.result.error)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                    
                }
                
            }
            
        }else{
        
            let dic = NSMutableDictionary.init()
            dic.setValue("No Internet Connection, try later!", forKey: "message")
            dic.setValue("false", forKey: "status")
            
            OnResultBlock(dic,"failure")

        }
        
        
        
    }
    
    
    class func callAPIWithAudio(parameter:NSDictionary,url:String,audioData:Data,filename:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        if checkInternetConnection() == true {
            upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
                
                multiPartFormData.append(audioData, withName: filename, fileName: filename, mimeType: "application/octet-stream")
                
                for (key, value) in parameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
                
            }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
                
                switch (encodingResult){
                    
                case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                    upload.responseJSON { response in
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let JSON = response.result.value
                        {
                            let dic = NSMutableDictionary.init()
                            dic.setValue("\(response.result)", forKey: "message")
                            OnResultBlock(dic,"Suceess")
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    
                    print(encodingError)
                    let dic = NSMutableDictionary.init()
                    dic.setValue("Connection Time Out", forKey: "message")
                    OnResultBlock(dic,"failure")
                    
                }
                
            }
            
        }else{
            let dic = NSMutableDictionary.init()
            dic.setValue("No Internet Connection, try later!", forKey: "message")
            dic.setValue("false", forKey: "status")
            OnResultBlock(dic,"failure")

        
        
        }
        
    }


    
    class func callAPIWithImageOnlyOne(parameter:NSDictionary,url:String,image:UIImage,imageName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        if checkInternetConnection() == true {
            upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
                
                
                
                    if let imageData = UIImageJPEGRepresentation(image, 0.8) {
                        multiPartFormData.append(imageData, withName: imageName, fileName: imageName, mimeType: "jpg/jpeg")
                       }
                
           
                
                for (key, value) in parameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
                
            }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
                
                switch (encodingResult){
                    
                case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                    upload.responseJSON { response in
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let JSON = response.result.value
                        {
                            let dic = NSMutableDictionary.init()
                            dic.setValue("\(response.result)", forKey: "message")
                            OnResultBlock(dic,"Suceess")
                        }else{
                            let dic = NSMutableDictionary.init()
                            dic.setValue("FAILURE", forKey: "message")
                            OnResultBlock(dic,"failure")
                        
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    
                    print(encodingError)
                    let dic = NSMutableDictionary.init()
                    dic.setValue("Connection Time Out", forKey: "message")
                    OnResultBlock(dic,"failure")
                    
                }
                
            }
            
        }else{
        
            
            let dic = NSMutableDictionary.init()
            dic.setValue("No Internet Connection, try later!", forKey: "message")
            dic.setValue("false", forKey: "status")
            OnResultBlock(dic,"failure")
        
        
        }
        
    }

    
    //MARK:
    //MARK: API With Secureity Parameter
    func callSecurityAPI(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        let r = Reachability(hostname: "www.google.com")
        
        if r?.currentReachabilityStatus != Reachability.NetworkStatus.reachableViaWiFi && r?.currentReachabilityStatus==Reachability.NetworkStatus.reachableViaWWAN {
            
            let dic = NSMutableDictionary.init()
            dic.setValue("No Internet Connection, try later!", forKey: "message")
            dic.setValue("false", forKey: "status")
            
            OnResultBlock(dic,"failure")
        }
        else
        {
            let finalParameter = self.addSecurity_Parameter(parameter: parameter as! NSMutableDictionary)
            
            request(url, method: .post, parameters: finalParameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                    
                case .success(_):
                    if let data = response.result.value{
                        OnResultBlock(data as! NSDictionary,"Suceess")
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                    
                }
                
            }
            
        }
        
    }
    
    func callAPIWithSecurity_Image(parameter:NSDictionary,url:String,image:UIImage,imageName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        let r = Reachability(hostname: "www.google.com")
        
        if r?.currentReachabilityStatus != Reachability.NetworkStatus.reachableViaWiFi && r?.currentReachabilityStatus==Reachability.NetworkStatus.reachableViaWWAN {
            
            let dic = NSMutableDictionary.init()
            dic.setValue("No Internet Connection, try later!", forKey: "message")
            dic.setValue("false", forKey: "status")
            OnResultBlock(dic,"failure")
        }
        else
        {
            let finalParameter = self.addSecurity_Parameter(parameter: parameter as! NSMutableDictionary)
            
            upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
                
                if  let imageData = UIImageJPEGRepresentation(image, 0.5) {
                    let fileName = String(format: "%f.jpeg", NSDate.init().timeIntervalSince1970)
                    multiPartFormData.append(imageData, withName: imageName, fileName: fileName, mimeType: "image/jpeg")
                }
                
                for (key, value) in finalParameter {
                    multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                }
                
            }, to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
                
                switch (encodingResult){
                    
                case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                    upload.responseJSON { response in
                        
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let JSON = response.result.value
                        {
                            OnResultBlock(JSON as! NSDictionary,"Suceess")
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    
                    print(encodingError)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    
                }
                
            }
            
        }
        
    }
    
    //MARK:
    //MARK: Get Device ID
    func get_DeviceID() -> String {
        let device = UIDevice.current
        let str_deviceID = device.identifierForVendor?.uuidString
        return str_deviceID!
    }
    
    //MARK:
    //MARK: Get Current Time
    func get_currentTime() -> String
    {
        let timeFormat = DateFormatter.init()
        timeFormat.dateFormat = "DDYYYYHHmmss"
        return timeFormat.string(from: Date.init())
        
    }
    
    //MARK:
    //MARK: Get Current Time
    func addSecurity_Parameter(parameter:NSMutableDictionary) -> NSDictionary
    {
        let strTimeStamp = self.get_currentTime()
        let strDeviceType = "2"
        let str_Token = String.init(format: "%@%@%@%@", "masterkey","123456789",strDeviceType,strTimeStamp)
        let str_md5 = str_Token.md5
        
        parameter.setValue("123456789", forKey: "user_device_token")
        parameter.setValue(strDeviceType, forKey: "user_device_type")
        parameter.setValue(strTimeStamp, forKey: "timestamp")
        parameter.setValue(str_md5, forKey: "md5_key")
        
        return parameter
    }
    
}

//MARK:
//MARK: MD5 Generate
extension String  {
    var md5: String! {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.deallocate(capacity: digestLen)
        
        return String(format: hash as String)
    }
}
